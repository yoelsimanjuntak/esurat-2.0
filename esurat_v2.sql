/*
SQLyog Community v12.09 (64 bit)
MySQL - 10.1.19-MariaDB : Database - esurat_v2
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`esurat_v2` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `team1_esurat`;

/*Table structure for table `language` */

DROP TABLE IF EXISTS `language`;

CREATE TABLE `language` (
  `LanguageID` int(11) NOT NULL AUTO_INCREMENT,
  `LanguageName` varchar(200) NOT NULL,
  PRIMARY KEY (`LanguageID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `language` */

/*Table structure for table `mopd` */

DROP TABLE IF EXISTS `mopd`;

CREATE TABLE `mopd` (
  `KdOPD` bigint(10) NOT NULL AUTO_INCREMENT,
  `NmOPD` varchar(200) NOT NULL,
  PRIMARY KEY (`KdOPD`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `mopd` */

insert  into `mopd`(`KdOPD`,`NmOPD`) values (1,'Dinas Komunikasi dan Informatika'),(2,'Dinas Pendidikan'),(3,'Dinas Pariwisata');

/*Table structure for table `mstatus` */

DROP TABLE IF EXISTS `mstatus`;

CREATE TABLE `mstatus` (
  `KdStatus` bigint(10) NOT NULL,
  `KdTipe` bigint(10) NOT NULL,
  `NmStatus` varchar(100) NOT NULL,
  `RequireOPD` tinyint(1) DEFAULT NULL,
  `Remarks` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`KdStatus`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mstatus` */

insert  into `mstatus`(`KdStatus`,`KdTipe`,`NmStatus`,`RequireOPD`,`Remarks`) values (1,1,'Baru',NULL,NULL),(2,1,'Disposisi ke Asisten Pemerintahan',NULL,NULL),(3,1,'Disposisi ke Asisten Ek. Bang',NULL,NULL),(4,1,'Disposisi ke Asisten Adm. dan Kesra',NULL,NULL),(5,1,'Disposisi ke Sekda',NULL,NULL),(6,1,'Disposisi ke Bupati',NULL,NULL),(7,1,'Distribusi ke Ka. Inspektorat',NULL,NULL),(8,1,'Distribusi ke Ka. OPD',NULL,NULL),(9,1,'Distribusi ke Ka. RSU / Camat / UPT / Puskesmas',NULL,NULL),(10,1,'Distribusi ke Ka. Sekolah',NULL,NULL),(11,1,'Distribusi ke Irban',NULL,NULL),(12,1,'Distribusi ke Sekretaris OPD / Kabid',NULL,NULL),(13,1,'Distribusi ke Sekretaris Camat / Kasubbag / Kasi',NULL,NULL),(14,1,'Distribusi ke Auditor / Staff',NULL,NULL),(15,1,'Distribusi ke Kasubbag / Kasubbid / Kasi OPD',NULL,NULL),(16,1,'Distribusi ke Staff',NULL,NULL),(17,1,'Distribusi ke Staff OPD',NULL,NULL),(18,1,'Disposisi ke Op. Kepala Bagian Setdakab',NULL,NULL),(19,1,'Disposisi ke Kepala Bagian Setdakab',NULL,NULL),(20,1,'Disposisi ke Kasubbag Setdakab',NULL,NULL),(21,1,'Disposisi ke Staff Setdakab',NULL,NULL),(22,1,'Distribusi ke Operator OPD / Badan',1,NULL),(23,1,'Distribusi ke Operator Inspektorat',1,NULL),(24,1,'Distribusi ke Operator Camat / RSU / UPT / Puskesmas',1,NULL),(25,1,'Distribusi ke Operator Sekolah',1,NULL),(26,2,'Baru',NULL,NULL),(27,2,'Disposisi ke Kasubbag Setdakab',NULL,NULL),(28,2,'Disposisi ke Sekretaris / Irban',NULL,NULL),(29,2,'Disposisi ke Kasubbag / Kasubbid / Kasi OPD',NULL,NULL),(30,2,'Disposisi ke Sekretaris Camat / Kasubbag / Kasi',NULL,NULL),(31,2,'Disposisi ke Operator Sekolah',NULL,NULL),(32,2,'Disposisi ke Ka. Bagian Setdakab',NULL,NULL),(33,2,'Disposisi ke Camat / RSU / UPT / Puskesmas',NULL,NULL),(34,2,'Disposisi ke Sekretaris / Kabid OPD',NULL,NULL),(35,2,'Disposisi ke Ka. Inspektorat',NULL,NULL),(36,2,'Disposisi ke Ka. Dinas OPD / Badan',NULL,NULL),(37,2,'Disposisi ke Operator Ka. Bagian Setdakab',NULL,NULL),(38,2,'Disposisi ke Operator Inspektorat',NULL,NULL),(39,2,'Disposisi ke Operator Dinas OPD / Badan',NULL,NULL),(40,2,'Disposisi ke Operator Camat / RSU / UPT / Puskesmas',NULL,NULL),(41,2,'Disposisi ke Operator Bagian Umum',NULL,NULL),(42,2,'Disposisi ke Asisten Pemerintahan',NULL,NULL),(43,2,'Disposisi ke Asisten Ek. Bang',NULL,NULL),(44,2,'Disposisi ke Asisten Adm. dan Kesra',NULL,NULL),(45,2,'Disposisi ke Sekda',NULL,NULL),(46,2,'Disposisi ke Bupati / Wakil Bupati',NULL,NULL);

/*Table structure for table `mstatus_opsi` */

DROP TABLE IF EXISTS `mstatus_opsi`;

CREATE TABLE `mstatus_opsi` (
  `KdStatus` bigint(10) NOT NULL,
  `KdOpsi` bigint(10) NOT NULL,
  `KdRole` bigint(10) DEFAULT NULL,
  PRIMARY KEY (`KdStatus`,`KdOpsi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mstatus_opsi` */

insert  into `mstatus_opsi`(`KdStatus`,`KdOpsi`,`KdRole`) values (1,2,NULL),(1,3,NULL),(1,4,NULL),(2,5,NULL),(2,18,NULL),(2,22,NULL),(2,23,NULL),(2,24,NULL),(2,25,NULL),(3,5,NULL),(3,18,NULL),(3,22,NULL),(3,23,NULL),(3,24,NULL),(3,25,NULL),(4,5,NULL),(4,18,NULL),(4,22,NULL),(4,23,NULL),(4,24,NULL),(4,25,NULL),(5,2,NULL),(5,3,NULL),(5,4,NULL),(5,6,NULL),(5,22,NULL),(5,23,NULL),(5,24,NULL),(5,25,NULL),(6,5,NULL),(7,11,NULL),(8,12,NULL),(9,13,NULL),(11,14,NULL),(12,15,NULL),(13,16,NULL),(15,17,NULL),(18,19,NULL),(19,20,NULL),(20,21,NULL),(22,8,NULL),(23,7,NULL),(24,9,NULL),(25,10,NULL),(26,27,22),(26,28,15),(26,29,18),(26,30,17),(26,31,11),(27,32,NULL),(28,35,NULL),(29,34,NULL),(30,33,NULL),(31,22,NULL),(31,23,NULL),(31,24,NULL),(31,25,NULL),(31,41,NULL),(32,37,NULL),(33,40,NULL),(34,36,NULL),(35,38,NULL),(36,39,NULL),(37,22,NULL),(37,23,NULL),(37,24,NULL),(37,25,NULL),(37,41,NULL),(38,22,NULL),(38,23,NULL),(38,24,NULL),(38,25,NULL),(38,41,NULL),(39,22,NULL),(39,23,NULL),(39,24,NULL),(39,25,NULL),(39,41,NULL),(40,22,NULL),(40,23,NULL),(40,24,NULL),(40,25,NULL),(40,41,NULL),(41,42,NULL),(41,43,NULL),(41,44,NULL),(42,45,NULL),(43,45,NULL),(44,45,NULL),(45,31,NULL),(45,37,NULL),(45,38,NULL),(45,39,NULL),(45,40,NULL),(45,46,NULL),(46,45,NULL);

/*Table structure for table `mstatus_role` */

DROP TABLE IF EXISTS `mstatus_role`;

CREATE TABLE `mstatus_role` (
  `KdStatus` bigint(10) NOT NULL,
  `KdRole` bigint(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mstatus_role` */

insert  into `mstatus_role`(`KdStatus`,`KdRole`) values (1,2),(2,3),(3,4),(4,5),(5,6),(6,7),(7,8),(8,9),(9,10),(10,11),(11,12),(12,13),(13,14),(14,15),(15,16),(16,17),(17,18),(18,19),(19,20),(20,21),(21,22),(22,23),(23,24),(24,25),(25,26),(26,15),(26,17),(26,18),(26,22),(26,11),(27,21),(28,12),(29,16),(30,14),(31,26),(32,20),(33,10),(34,13),(35,8),(36,9),(37,19),(38,24),(39,23),(40,25),(41,2),(42,3),(43,4),(44,5),(45,6),(46,7);

/*Table structure for table `mtipe` */

DROP TABLE IF EXISTS `mtipe`;

CREATE TABLE `mtipe` (
  `KdTipe` bigint(10) NOT NULL,
  `NmTipe` varchar(50) NOT NULL,
  PRIMARY KEY (`KdTipe`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mtipe` */

insert  into `mtipe`(`KdTipe`,`NmTipe`) values (1,'Surat Masuk'),(2,'Surat Keluar');

/*Table structure for table `postcategories` */

DROP TABLE IF EXISTS `postcategories`;

CREATE TABLE `postcategories` (
  `PostCategoryID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryName` varchar(50) NOT NULL,
  PRIMARY KEY (`PostCategoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `postcategories` */

insert  into `postcategories`(`PostCategoryID`,`PostCategoryName`) values (1,'News'),(2,'Blog'),(3,'Event'),(4,'Gallery'),(5,'Others');

/*Table structure for table `posts` */

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `PostID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryID` int(10) NOT NULL,
  `PostDate` date NOT NULL,
  `PostTitle` varchar(200) NOT NULL,
  `PostSlug` varchar(200) NOT NULL,
  `PostContent` longtext NOT NULL,
  `PostExpiredDate` date NOT NULL,
  `TotalView` int(11) NOT NULL DEFAULT '0',
  `LastViewDate` datetime DEFAULT NULL,
  `IsSuspend` tinyint(1) NOT NULL DEFAULT '1',
  `FileName` varchar(250) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) NOT NULL,
  `UpdatedOn` datetime NOT NULL,
  PRIMARY KEY (`PostID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `posts` */

insert  into `posts`(`PostID`,`PostCategoryID`,`PostDate`,`PostTitle`,`PostSlug`,`PostContent`,`PostExpiredDate`,`TotalView`,`LastViewDate`,`IsSuspend`,`FileName`,`CreatedBy`,`CreatedOn`,`UpdatedBy`,`UpdatedOn`) values (1,1,'2018-10-15','Interdum et malesuada fames ac ante ipsum elementum vel lorem eu primis in faucibus','interdum-et-malesuada-fames-ac-ante-ipsum-elementum-vel-lorem-eu-primis-in-faucibus','<ul>\r\n	<li>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</li>\r\n	<li>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</li>\r\n</ul>\r\n','2020-01-01',14,NULL,0,'jan-ethes-dan-presiden-jokowi_20181007_090059.jpg','admin','2016-10-18 19:14:58','admin','2018-10-15 15:05:51'),(2,1,'2016-11-10','Swimmer Ryan Lochte wins endorsement deal for ‘forgiving’ throat drop','swimmer-ryan-lochte-wins-endorsement-deal-for-forgiving-throat-drop','<p>Integer rhoncus vestibulum lectus ac sodales. Vestibulum dapibus, magna quis finibus scelerisque, sapien nisl rutrum mauris, eget ullamcorper est purus pharetra orci. Nam eu magna sit amet dui convallis rhoncus non vel odio. Phasellus in libero nec nunc venenatis finibus sed in neque.Nullam auctor, quam ut eleifend hendrerit, tellus mauris dignissim nibh, nec euismod felis odio ac ex. Maecenas finibus lacinia fermentum.Vestibulum hendrerit, mauris vel convallis semper, ante purus vehicula diam, non blandit leo leo ac justo. Praesent viverra, lacus et tempus tincidunt, massa eros eleifend massa, ac tempus ipsum justo vel erat.Mauris vitae magna lacinia, vehicula diam sed, rutrum tortor. Maecenas orci nibh, tincidunt quis eros ac, vehicula rhoncus lacus. Maecenas ut justo sit amet lectus consequat mollis. Phasellus vehicula consequat vehicula.<em>&quot;Fusce nulla turpis, tempor at auctor et, dignissim semper ligula. Cras eu dolor blandit, facilisis mi et, ultrices orci sapien nisl rutrum mauris, eget ullamcorper est purus pharetra orci. Nam eu magna sit amet dui convallis rhoncus non vel odio. Phasellus in libero nec nunc venenatis finibus sed in neque.</em>Vestibulum hendrerit, mauris vel convallis semper, ante purus vehicula diam, non blandit leo leo ac justo. Praesent viverra, lacus et tempus tincidunt, massa eros eleifend massa, ac tempus ipsum justo vel erat.</p>\r\n','2020-01-01',25,NULL,0,NULL,'admin','2016-10-18 20:16:10','admin','2016-11-10 02:36:42'),(3,3,'2016-11-10','U.S. Navy ship fires warning shots at Iranian vessel','u-s-navy-ship-fires-warning-shots-at-iranian-vessel','<ul>\r\n	<li>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</li>\r\n	<li>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</li>\r\n</ul>\r\n','2020-01-01',3,NULL,0,NULL,'admin','2016-10-18 20:25:16','admin','2016-11-10 02:36:31'),(4,5,'2016-10-22','Contact Us','contact-us','<p>DEL dimaknai sebagai `Pemimpin yang Selangkah Lebih Maju`.</p>\r\n\r\n<p>Yayasan Del mulai aktif di kegiatan sosial kemasyarakatan jauh sebelum didirikannya PT Toba Sejahtera, perusahaan yang kemudian menjadi Yayasan Del sebagai tonggak tanggung jawab sosial perusahaan.</p>\r\n\r\n<p>Tujuan awal Yayasan Del didirikan adalah untuk memberikan akses pendidikan berkualitas di daerah terpencil bagi siswa/i berprestasi dengan latar belakang ekonomi yang kurang menguntungkan, yaitu dengan mendirikan Politeknik Informatika Del dan SMA Unggul Del di Laguboti, Sumatera Utara serta Sekolah NOAH di Kalisari, Jakarta Timur.</p>\r\n\r\n<p>Tidak hanya berkiprah di bidang pendidikan, Yayasan Del juga aktif bekerjasama dengan pemerintah daerah dan lembaga sosial yang ada di Indonesia untuk meningkatkan pelayanan serta memperluas cakrawala bidang pelayanan strategis lainnya.</p>\r\n\r\n<p>Politeknik Informatika Del didirikan pada tahun 2001 dan bertujuan untuk menyediakan pendidikan tinggi berkualitas internasional, bagi siswa/i berpotensi, terutama dengan latar belakang ekonomi yang kurang menguntungkan, khususnya yang berasal dari Sumatera Utara.</p>\r\n\r\n<p>Disamping turut berperan sebagai inisiator penggerak pembangunan di Tapanuli, IT Del juga diharapkan dapat menginkubasi lahirnya para&nbsp;<em>technopreneur</em>&nbsp;baru di bidang teknologi informasi.</p>\r\n\r\n<p>Dengan lokasi di daerah tepian Danau Toba, berjarak sekitar 400 KM dari kota Medan, area IT Del diharapkan dapat memberikan suasana tenang dan kondusif dalam belajar, karena jauh dari kebisingan dan hiruk pikuk suasana perkotaan/perindustrian.</p>\r\n','2020-01-01',0,NULL,0,NULL,'admin','2016-10-22 20:23:03','admin','2016-10-22 20:23:03'),(5,5,'2016-10-22','FAQ','faq','<blockquote>\r\n<ol>\r\n	<li><strong>Saya mahasiswa / alumni IT DEL, apakah saya perlu mendaftar sebagai member Del Career Center?</strong></li>\r\n</ol>\r\n</blockquote>\r\n\r\n<blockquote>\r\n<ul>\r\n	<li>Setiap mahasiswa dan alumni IT DEL&nbsp;secara otomatis terdaftar sebagai member Del Career Center. Untuk informasi lebih lanjut, silahkan hubungi Administrator</li>\r\n</ul>\r\n</blockquote>\r\n','2020-01-01',5,NULL,0,NULL,'admin','2016-10-22 20:26:38','admin','2016-10-22 20:29:34'),(6,4,'2018-10-15','Foto 1','foto-1','<p>Test Gallery</p>\r\n','2020-01-01',3,NULL,0,'jan-ethes-dan-presiden-jokowi_20181007_0900591.jpg','admin','2018-10-15 15:45:16','admin','2018-10-15 15:45:16'),(7,4,'2018-10-15','Foto 2','foto-2','<p>Optimus!</p>\r\n','2020-01-01',13,NULL,0,'autobots_logo_by_theseventhshadow.jpg','admin','2018-10-15 15:59:20','admin','2018-10-15 15:59:20'),(8,4,'2018-10-15','Foto 3','foto-3','<p>Foto 3</p>\r\n','2020-01-01',1,NULL,0,'boss-vs-leader.png','admin','2018-10-15 17:15:14','admin','2018-10-15 17:15:14'),(9,4,'2018-10-15','Foto 4','foto-4','<p>Test</p>\r\n','2020-01-01',0,NULL,0,'autobots_logo_by_theseventhshadow1.jpg','admin','2018-10-15 17:17:57','admin','2018-10-15 17:17:57');

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `RoleID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(50) NOT NULL,
  PRIMARY KEY (`RoleID`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

/*Data for the table `roles` */

insert  into `roles`(`RoleID`,`RoleName`) values (1,'Administrator'),(2,'Bagian Umum'),(3,'Asisten Pemerintahan'),(4,'Asisten Ek. Bang'),(5,'Asisten Adm & Kesra'),(6,'Sekretaris Daerah'),(7,'Bupati / Wakil Bupati'),(8,'Ka. Inspektorat'),(9,'Ka. OPD / Badan'),(10,'Camat / Ka. RSU / UPT / Puskesmas'),(11,'Ka. Sekolah'),(12,'Sekretaris / Irban'),(13,'Sekretaris / Kabid OPD'),(14,'Sekretaris Camat / Kasubbag / Kasi'),(15,'Auditor / Staff'),(16,'Kasubbag / Kasubbid / Kasi OPD'),(17,'Staff Camat'),(18,'Staff OPD'),(19,'Opr. Ka. Bagian Setdakab'),(20,'Ka. Bagian Setdakab'),(21,'Kasubbag Setdakab'),(22,'Staff Setdakab'),(23,'Opr. OPD / Badan'),(24,'Opr. Inspektorat'),(25,'Opr. RSU / Camat / UPT / Puskesmas'),(26,'Opr. Sekolah');

/*Table structure for table `settings` */

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `SettingID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`SettingID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `settings` */

/*Table structure for table `tsurat` */

DROP TABLE IF EXISTS `tsurat`;

CREATE TABLE `tsurat` (
  `KdSurat` bigint(10) NOT NULL AUTO_INCREMENT,
  `KDSumber` bigint(10) DEFAULT NULL,
  `KdOPD` bigint(10) DEFAULT NULL,
  `KdTipe` bigint(10) NOT NULL,
  `KdStatus` bigint(10) NOT NULL,
  `NmJudul` varchar(200) NOT NULL,
  `NmPerihal` varchar(200) DEFAULT NULL,
  `NmFile` varchar(200) DEFAULT NULL,
  `CreatedAt` datetime NOT NULL,
  `CreatedBy` varchar(200) NOT NULL,
  `ModifiedAt` datetime DEFAULT NULL,
  `ModifiedBy` varchar(200) DEFAULT NULL,
  `Deleted` tinyint(1) DEFAULT NULL,
  `DeletedAt` datetime DEFAULT NULL,
  `DeletedBy` varchar(200) DEFAULT NULL,
  `ForwardedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`KdSurat`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

/*Data for the table `tsurat` */

insert  into `tsurat`(`KdSurat`,`KDSumber`,`KdOPD`,`KdTipe`,`KdStatus`,`NmJudul`,`NmPerihal`,`NmFile`,`CreatedAt`,`CreatedBy`,`ModifiedAt`,`ModifiedBy`,`Deleted`,`DeletedAt`,`DeletedBy`,`ForwardedAt`) values (37,NULL,3,2,39,'11112087','DEVELOPMENT..','TAUFIK_INDRAWAN_MULYA_(1).pdf','2018-12-16 08:16:40','staff_pariwisata',NULL,NULL,NULL,NULL,NULL,'2018-12-16 08:23:53'),(38,37,2,1,12,'11112087','DEVELOPMENT..','TAUFIK_INDRAWAN_MULYA_(1).pdf','2018-12-16 08:16:40','staff_pariwisata',NULL,NULL,NULL,NULL,NULL,'2018-12-16 09:19:01'),(39,NULL,NULL,1,5,'Tel.142/YN 000/R1W-1A00000/2018','TEST','TAUFIK_INDRAWAN_MULYA_(1)1.pdf','2018-12-16 10:54:55','11112087',NULL,NULL,NULL,NULL,NULL,'2018-12-16 10:59:46'),(40,NULL,NULL,1,1,'Lorem Ipsum','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type a','androidstudiomastery.pdf','2018-12-22 08:19:09','11112087',NULL,NULL,NULL,NULL,NULL,NULL),(41,NULL,NULL,1,1,'Lorem Ipsum 2','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type a','EbookSqliteRevisi.pdf','2018-12-22 08:20:27','11112087',NULL,NULL,NULL,NULL,NULL,NULL),(42,NULL,NULL,1,1,'Lorem Ipsum 3','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type a','surat_penawaran2.pdf','2018-12-22 08:21:52','11112087',NULL,NULL,NULL,NULL,NULL,NULL),(43,NULL,NULL,1,1,'Section 1.10.32 of \"de Finibus Bonorum et Malorum\", written by Cicero in 45 BC','\"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta','1715751391-1545126411.pdf','2018-12-22 08:23:26','11112087',NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `tsurat_komentar` */

DROP TABLE IF EXISTS `tsurat_komentar`;

CREATE TABLE `tsurat_komentar` (
  `KdKomentar` bigint(10) NOT NULL AUTO_INCREMENT,
  `KdSurat` bigint(10) NOT NULL,
  `KdOPD` bigint(10) DEFAULT NULL,
  `NmKomentar` text,
  `NmFile` varchar(200) DEFAULT NULL,
  `CreatedAt` datetime NOT NULL,
  `CreatedBy` varchar(200) NOT NULL,
  PRIMARY KEY (`KdKomentar`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;

/*Data for the table `tsurat_komentar` */

insert  into `tsurat_komentar`(`KdKomentar`,`KdSurat`,`KdOPD`,`NmKomentar`,`NmFile`,`CreatedAt`,`CreatedBy`) values (48,37,3,'OK..',NULL,'2018-12-16 08:16:56','staff_pariwisata'),(49,37,3,'OK..',NULL,'2018-12-16 08:17:29','kasubbag_pariwisata'),(50,37,3,'OK..',NULL,'2018-12-16 08:18:40','kabid_pariwisata'),(51,37,3,'OK..',NULL,'2018-12-16 08:19:51','kadis_pariwisata'),(52,37,3,'OK..',NULL,'2018-12-16 08:20:22','opr_pariwisata'),(53,37,NULL,'OK..',NULL,'2018-12-16 08:21:16','11112087'),(54,37,NULL,'OK..',NULL,'2018-12-16 08:21:57','asisten1'),(55,37,NULL,'OK..',NULL,'2018-12-16 08:22:46','sekda'),(56,37,3,'OK..',NULL,'2018-12-16 08:23:48','opr_pariwisata'),(57,38,2,'OK..',NULL,'2018-12-16 09:17:51','opr_pendidikan'),(58,38,2,'OK..',NULL,'2018-12-16 09:18:56','kadis_pendidikan'),(59,39,NULL,'OK..',NULL,'2018-12-16 10:55:38','11112087'),(60,39,NULL,'OK..',NULL,'2018-12-16 10:59:44','asisten1');

/*Table structure for table `tsurat_lampiran` */

DROP TABLE IF EXISTS `tsurat_lampiran`;

CREATE TABLE `tsurat_lampiran` (
  `KdLampiran` bigint(10) NOT NULL AUTO_INCREMENT,
  `KdSurat` bigint(10) NOT NULL,
  `NmDeskripsi` varchar(200) NOT NULL,
  `NmFile` varchar(200) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `CreatedBy` varchar(200) NOT NULL,
  PRIMARY KEY (`KdLampiran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tsurat_lampiran` */

/*Table structure for table `tsurat_logstat` */

DROP TABLE IF EXISTS `tsurat_logstat`;

CREATE TABLE `tsurat_logstat` (
  `KdLog` bigint(10) NOT NULL AUTO_INCREMENT,
  `KdSurat` bigint(10) NOT NULL,
  `KdStatus` bigint(10) NOT NULL,
  `CreatedAt` datetime NOT NULL,
  `CreatedBy` varchar(200) NOT NULL,
  PRIMARY KEY (`KdLog`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=latin1;

/*Data for the table `tsurat_logstat` */

insert  into `tsurat_logstat`(`KdLog`,`KdSurat`,`KdStatus`,`CreatedAt`,`CreatedBy`) values (74,37,26,'2018-12-16 08:16:40','staff_pariwisata'),(75,37,29,'2018-12-16 08:17:00','staff_pariwisata'),(76,37,34,'2018-12-16 08:17:34','kasubbag_pariwisata'),(77,37,36,'2018-12-16 08:18:45','kabid_pariwisata'),(78,37,39,'2018-12-16 08:19:53','kadis_pariwisata'),(79,37,41,'2018-12-16 08:20:35','opr_pariwisata'),(80,37,44,'2018-12-16 08:21:19','11112087'),(81,37,45,'2018-12-16 08:22:00','asisten1'),(82,37,39,'2018-12-16 08:22:53','sekda'),(83,37,22,'2018-12-16 08:23:53','opr_pariwisata'),(84,38,8,'2018-12-16 09:18:04','opr_pendidikan'),(85,38,12,'2018-12-16 09:19:01','kadis_pendidikan'),(86,39,1,'2018-12-16 10:54:55','11112087'),(87,39,4,'2018-12-16 10:58:18','11112087'),(88,39,5,'2018-12-16 10:59:46','asisten1'),(89,40,1,'2018-12-22 08:19:09','11112087'),(90,41,1,'2018-12-22 08:20:27','11112087'),(91,42,1,'2018-12-22 08:21:52','11112087'),(92,43,1,'2018-12-22 08:23:26','11112087');

/*Table structure for table `tsurat_opd` */

DROP TABLE IF EXISTS `tsurat_opd`;

CREATE TABLE `tsurat_opd` (
  `KdSurat` bigint(10) NOT NULL,
  `KdOPD` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tsurat_opd` */

/*Table structure for table `userinformation` */

DROP TABLE IF EXISTS `userinformation`;

CREATE TABLE `userinformation` (
  `UserName` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `CompanyID` varchar(200) DEFAULT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `IdentityNo` varchar(50) DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `ReligionID` int(10) DEFAULT NULL,
  `Gender` tinyint(1) DEFAULT NULL,
  `Address` text,
  `PhoneNumber` varchar(50) DEFAULT NULL,
  `EducationID` int(10) DEFAULT NULL,
  `UniversityName` varchar(50) DEFAULT NULL,
  `FacultyName` varchar(50) DEFAULT NULL,
  `MajorName` varchar(50) DEFAULT NULL,
  `IsGraduated` tinyint(1) NOT NULL DEFAULT '0',
  `GraduatedDate` date DEFAULT NULL,
  `YearOfExperience` int(10) DEFAULT NULL,
  `RecentPosition` varchar(250) DEFAULT NULL,
  `RecentSalary` double DEFAULT NULL,
  `ExpectedSalary` double DEFAULT NULL,
  `CVFilename` varchar(250) DEFAULT NULL,
  `ImageFilename` varchar(250) DEFAULT NULL,
  `RegisteredDate` date DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `userinformation` */

insert  into `userinformation`(`UserName`,`Email`,`CompanyID`,`Name`,`IdentityNo`,`BirthDate`,`ReligionID`,`Gender`,`Address`,`PhoneNumber`,`EducationID`,`UniversityName`,`FacultyName`,`MajorName`,`IsGraduated`,`GraduatedDate`,`YearOfExperience`,`RecentPosition`,`RecentSalary`,`ExpectedSalary`,`CVFilename`,`ImageFilename`,`RegisteredDate`) values ('11112087','yoel.simanjuntak@id.wilmar-intl.com',NULL,'YOEL ROLAS SIMANJUNTAK',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-12-01'),('admin','admin@sidasma.humbanghasundutankab.go.id',NULL,'Administrator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-17'),('asisten1','asisten.adm.setdakab@humbanghasundutankab.go.id',NULL,'PASTAP MARULAP-ULAP',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-12-01'),('bupati','bupati@humbanghasundutankab.go.id',NULL,'BUPATI',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-12-01'),('kabid_pariwisata','kabid.pariwisata@humbanghasundutankab.go.id','3','KABID PARIWISATA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-12-02'),('kabid_pendidikan','kabid.pendidikan@humbanghasundutankab.go.id','2','KABID PENDIDIKAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-12-02'),('kadis_kominfo','kadis.kominfo@humbanghasundutankab.go.id','1','KADIS KOMINFO',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-12-02'),('kadis_pariwisata','kadis.pariwisata@humbanghasundutankab.go.id','3','KADIS PARIWISATA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-12-02'),('kadis_pendidikan','kadis.pendidikan@humbanghasundutankab.go.id','2','KADIS PENDIDIKAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-12-02'),('kasubbag_pariwisata','kasubbag_pariwisata@humbanghasundutankab.go.id','3','KASUBBAG DINAS PARIWISATA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-12-10'),('opr_pariwisata','opr_pariwisata@humbanghasundutankab.go.id','3','OPR. DINAS PARIWISATA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-12-10'),('opr_pendidikan','opr_pendidikan@humbanghasundutankab.go.id','2','OPR. DINAS PENDIDIKAN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-12-10'),('sekda','sekda.setdakab@humbanghasundutankab.go.id',NULL,'PAK SEKDA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-12-01'),('staff_pariwisata','staff_pariwisata@humbanghasundutankab.go.id','3','STAFF DINAS PARIWISATA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-12-10');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `RoleID` int(10) unsigned NOT NULL,
  `IsSuspend` tinyint(1) unsigned NOT NULL,
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`UserName`,`Password`,`RoleID`,`IsSuspend`,`LastLogin`,`LastLoginIP`) values ('11112087','e10adc3949ba59abbe56e057f20f883e',2,0,'2018-12-30 03:09:22','::1'),('admin','e10adc3949ba59abbe56e057f20f883e',1,0,'2018-12-30 03:04:34','::1'),('asisten1','e10adc3949ba59abbe56e057f20f883e',5,0,'2018-12-16 10:59:18','::1'),('bupati','e10adc3949ba59abbe56e057f20f883e',7,0,'2018-12-16 11:05:22','::1'),('kabid_pariwisata','e10adc3949ba59abbe56e057f20f883e',13,0,'2018-12-16 08:18:08','::1'),('kabid_pendidikan','e10adc3949ba59abbe56e057f20f883e',13,0,'2018-12-16 10:08:57','::1'),('kader1','e10adc3949ba59abbe56e057f20f883e',2,0,'2018-10-01 17:12:28','::1'),('kadis_kominfo','e10adc3949ba59abbe56e057f20f883e',9,0,'2018-12-02 10:24:34','::1'),('kadis_pariwisata','e10adc3949ba59abbe56e057f20f883e',9,0,'2018-12-16 08:19:39','::1'),('kadis_pendidikan','e10adc3949ba59abbe56e057f20f883e',9,0,'2018-12-16 09:18:25','::1'),('kasubbag_pariwisata','e10adc3949ba59abbe56e057f20f883e',16,0,'2018-12-16 08:17:15','::1'),('opr_pariwisata','e10adc3949ba59abbe56e057f20f883e',23,0,'2018-12-30 03:09:59','::1'),('opr_pendidikan','e10adc3949ba59abbe56e057f20f883e',23,0,'2018-12-16 10:09:31','::1'),('rirismanik','bbfb3b97637d3caa18d4f73c6bf1b3b6',2,0,'2018-08-18 18:18:59','127.0.0.1'),('sekda','e10adc3949ba59abbe56e057f20f883e',6,0,'2018-12-16 08:22:21','::1'),('staff_pariwisata','e10adc3949ba59abbe56e057f20f883e',18,0,'2018-12-16 08:16:18','::1'),('yoelrolas','bbfb3b97637d3caa18d4f73c6bf1b3b6',2,0,'2018-08-22 14:15:47','::1');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
