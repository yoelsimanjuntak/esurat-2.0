<?php
define('SITENAME', 'E-SURAT');

// Roles
define('ROLEADMIN', 1);
define('ROLECOMPANY', -1);
define('ROLEUSER', 2);

// Settings
define('SETTING_WEBMAIL', 'Webmail');
define('SETTING_EMAILSENDER', 'EmailSender');
define('SETTING_EMAILSENDERNAME', 'EmailSenderName');
define('SETTING_EMAILPROTOCOL', 'EmailProtocol');
define('SETTING_SMTPHOST', 'SMTPHost');
define('SETTING_SMTPPORT', 'SMTPPort');
define('SETTING_SMTPPASSWORD', 'SMTPPassword');
define('SETTING_SMTPEMAIL', 'SMTPEmail');

// Email
//define('EMAILPROTOCOL',GetSetting(SETTING_EMAILPROTOCOL));
//define('SMTPHOST',GetSetting(SETTING_SMTPHOST));
//define('SMTPPORT',GetSetting(SETTING_SMTPPORT));
//define('SMTPEMAIL',GetSetting(SETTING_SMTPEMAIL));
//define('SMTPPASSWORD',GetSetting(SETTING_SMTPPASSWORD));

// Notifications
define('NOTIFICATION_PERUSAHAANBARU', 1);
define('NOTIFICATION_AKTIVASIAKUNPERUSAHAAN', 2);
define('NOTIFICATION_LOWONGANBARUADMIN', 3);
define('NOTIFICATION_LOWONGANBARUUSER', 4);
define('NOTIFICATION_LAMARANBARUPERUSAHAAN', 5);
define('NOTIFICATION_LAMARANBARUUSER', 6);
define('NOTIFICATION_LAMARANDIRESPONUSER', 7);
define('NOTIFICATION_LUPAPASSWORD', 8);

// Post
define('POSTCATEGORY_NEWS', 1);
define('POSTCATEGORY_BLOG', 2);
define('POSTCATEGORY_EVENT', 3);

// Status
define('STATUS_DIPROSES', 1);
define('STATUS_DITERIMA', 2);
define('STATUS_DITOLAK', 3);

// Preference Type
define('PREFERENCETYPE_INDUSTRYTYPE', 1);
define('PREFERENCETYPE_EDUCATIONTYPE', 2);
define('PREFERENCETYPE_POSITION', 3);
define('PREFERENCETYPE_LOCATION', 4);
define('PREFERENCETYPE_VACANCYTYPE', 5);

define('MODE_CREATE', "buat");
define('MODE_EDIT', "ubah");
define('MODE_DELETE', "hapus");
define('MODE_VIEW', "lihat");
define('MODE_FORWARD', "fwd");

// Tipe Surat
define('SURAT_MASUK', 1);
define('SURAT_KELUAR', 2);
define('SURAT_STAT_BARU', 1);
define('SURAT_KELUAR_STAT_BARU', 26);
