<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 22/08/2018
 * Time: 00:31
 */
?>
    <head>
        <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/dist/css/AdminLTE.min.css">
    </head>
    <style>
        table.with-border {
            border-collapse: collapse;
        }

        table.with-border, th.with-border, td.with-border {
            border: 1px solid black;
        }

        .table-bordered>thead>tr>th {
            text-align: center !important;
        }

        .table>thead:first-child>tr:first-child>th {
            border-top: 0;
        }*/

        .table.no-padding>tbody>tr>td {
            padding: 3px !important;
            border-top: none !important;
        }

        body {
            font-size: 6.5pt !important;
        }

        .text-med {
            font-size: 10pt;
        }

        .text-lg {
            font-size: 12pt;
        }
    </style>
<?php

?>
<div>
    <div style="text-align: center">
        <p style="font-size: 14px; margin: 0px">REKAPITULASI</p>
        <p style="font-size: 14px; margin: 0px">CATATAN DATA DAN KEGIATAN WARGA KELOMPOK PKK RT</p>
    </div>

    <div style="margin-top: 10px">
        <table class="table no-padding text-med" style="width: 100%">
            <tr>
                <td style="width: 49%; text-align: right; padding: 3px">RT / RW</td>
                <td style="width: 1%">:</td>
                <td style="width: 49%; text-align: left"></td>
            </tr>
            <tr>
                <td style="width: 49%; text-align: right; padding: 3px">Desa / Kelurahan</td>
                <td style="width: 1%">:</td>
                <td style="width: 49%; text-align: left"><?=$dat[COL_NMKELURAHAN]?></td>
            </tr>
            <tr>
                <td style="width: 49%; text-align: right; padding: 3px">Tahun</td>
                <td style="width: 1%">:</td>
                <td style="width: 49%; text-align: left"><?=date("Y")?></td>
            </tr>
        </table>
    </div>
    <div class="margin-top: 10px">
        <table class="table table-bordered">
            <thead style="text-align: center">
            <tr>
                <th style="padding: 5px;text-align: center" rowspan="2">No.</th>
                <th style="padding: 5px;text-align: center" rowspan="2">NAMA DASAWISMA</th>
                <th style="padding: 5px;text-align: center" rowspan="2" width="10px">JLH. KRT</th>
                <th style="padding: 5px;text-align: center" rowspan="2" width="10px">JLH. KK</th>
                <th style="padding: 5px;text-align: center" colspan="2">TOTAL</th>
                <th style="padding: 5px;text-align: center" colspan="2">BALITA</th>
                <th style="padding: 5px;text-align: center" colspan="7" rowspan="2">ANGGOTA KELUARGA</th>
                <th style="padding: 5px;text-align: center" colspan="6" rowspan="2">KRITERIA RUMAH</th>
                <th style="padding: 5px;text-align: center" colspan="3" rowspan="2" width="40px">SUMBER AIR KELUARGA</th>
                <th style="padding: 5px;text-align: center" colspan="2" rowspan="2" width="40px">MAKANAN POKOK</th>
                <th style="padding: 5px;text-align: center" colspan="5" rowspan="2">MENGIKUTI KEGIATAN</th>
            </tr>
            <tr>
                <th style="padding: 5px;text-align: center">L</th>
                <th style="padding: 5px;text-align: center">P</th>
                <th style="padding: 5px;text-align: center">L</th>
                <th style="padding: 5px;text-align: center">P</th>
            </tr>
            <tr>
                <th style="padding: 5px;text-align: center">(1)</th>
                <th style="padding: 5px;text-align: center">(2)</th>
                <th style="padding: 5px;text-align: center">(3)</th>
                <th style="padding: 5px;text-align: center">(3)</th>
                <th style="padding: 5px;text-align: center">(4)</th>
                <th style="padding: 5px;text-align: center">(5)</th>
                <th style="padding: 5px;text-align: center">(6)</th>
                <th style="padding: 5px;text-align: center">(7)</th>
                <th style="padding: 5px;text-align: center">(8)</th>
                <th style="padding: 5px;text-align: center">(9)</th>
                <th style="padding: 5px;text-align: center">(10)</th>
                <th style="padding: 5px;text-align: center">(11)</th>
                <th style="padding: 5px;text-align: center">(12)</th>
                <th style="padding: 5px;text-align: center">(13)</th>
                <th style="padding: 5px;text-align: center">(14)</th>
                <th style="padding: 5px;text-align: center">(15)</th>
                <th style="padding: 5px;text-align: center">(16)</th>
                <th style="padding: 5px;text-align: center">(17)</th>
                <th style="padding: 5px;text-align: center">(18)</th>
                <th style="padding: 5px;text-align: center">(19)</th>
                <th style="padding: 5px;text-align: center">(20)</th>
                <th style="padding: 5px;text-align: center">(21)</th>
                <th style="padding: 5px;text-align: center">(22)</th>
                <th style="padding: 5px;text-align: center">(23)</th>
                <th style="padding: 5px;text-align: center">(24)</th>
                <th style="padding: 5px;text-align: center">(25)</th>
                <th style="padding: 5px;text-align: center">(26)</th>
                <th style="padding: 5px;text-align: center">(27)</th>
                <th style="padding: 5px;text-align: center">(28)</th>
                <th style="padding: 5px;text-align: center">(29)</th>
                <th style="padding: 5px;text-align: center">(30)</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $nourut = 1;
            $gt = array(
                3=>0,
                4=>0,
                5=>0,
                6=>0,
                7=>0,
                8=>0,
                9=>0,
                10=>0,
                11=>0,
                12=>0,
                13=>0,
                14=>0,
                15=>0,
                16=>0,
                17=>0,
                18=>0,
                19=>0,
                20=>0,
                21=>0,
                22=>0,
                23=>0,
                24=>0,
                25=>0,
                26=>0,
                27=>0,
                28=>0,
                29=>0,
                30=>0
            );
            foreach($res as $dat) {
                $kk = $this->db
                    ->select('*,
                        (SELECT COUNT(*) FROM mwarga w WHERE w.KdKeluarga=mkeluarga.KdKeluarga and w.Jeniskelamin=\'Laki-Laki\' and EXISTS (select mwargameninggal.NIK from mwargameninggal where mwargameninggal.NIK = w.NIK) = false) AS COUNT_LK,
                        (SELECT COUNT(*) FROM mwarga w WHERE w.KdKeluarga=mkeluarga.KdKeluarga and w.Jeniskelamin=\'Perempuan\' and EXISTS (select mwargameninggal.NIK from mwargameninggal where mwargameninggal.NIK = w.NIK) = false) AS COUNT_PR,
                        (SELECT COUNT(*) FROM mwarga w WHERE w.KdKeluarga=mkeluarga.KdKeluarga and w.StatusKeluarga=\'Suami\' and EXISTS (select mwargameninggal.NIK from mwargameninggal where mwargameninggal.NIK = w.NIK) = false) AS COUNT_KK,
                        (SELECT COUNT(*) FROM mkegiatan a where a.KdKeluarga = mkeluarga.KdKeluarga and KdTipe = 1) AS COUNT_TANAH,
                        (SELECT COUNT(*) FROM mkegiatan a where a.KdKeluarga = mkeluarga.KdKeluarga and KdTipe = 2) AS COUNT_INDUSTRI,
                        (SELECT COUNT(*) FROM maktifitas a where a.NIK in (select w.NIK from mwarga w where w.KdKeluarga = mkeluarga.KdKeluarga) and IsKeg2 = 1) AS COUNT_KB,
                        (SELECT COUNT(*) FROM maktifitas a where a.NIK in (select w.NIK from mwarga w where w.KdKeluarga = mkeluarga.KdKeluarga) and IsKeg2 = 7) AS COUNT_LL
                        ')
                    ->where(COL_KDDASAWISMA, $dat[COL_KDDASAWISMA])
                    ->order_by(COL_KDKELUARGA, 'asc')
                    ->get(TBL_MKELUARGA)->result_array();
                //$q =  $this->db->last_query();
                $no = 1;
                $total = array(
                    3=>0,
                    4=>0,
                    5=>0,
                    6=>0,
                    7=>0,
                    8=>0,
                    9=>0,
                    10=>0,
                    11=>0,
                    12=>0,
                    13=>0,
                    14=>0,
                    15=>0,
                    16=>0,
                    17=>0,
                    18=>0,
                    19=>0,
                    20=>0,
                    21=>0,
                    22=>0,
                    23=>0,
                    24=>0,
                    25=>0,
                    26=>0,
                    27=>0,
                    28=>0,
                    29=>0,
                    30=>0
                );
                foreach($kk as $d) {
                    $countBalitaLK = $this->db
                        ->where(COL_KDKELUARGA, $d[COL_KDKELUARGA])
                        ->where("TIMESTAMPDIFF(YEAR,TanggalLahir,CURDATE()) > 0 and TIMESTAMPDIFF(YEAR,TanggalLahir,CURDATE()) <= 4")
                        ->where(COL_JENISKELAMIN, "Laki-Laki")
                        ->where("EXISTS (select mwargameninggal.NIK from mwargameninggal where mwargameninggal.NIK = mwarga.NIK) = false")
                        ->count_all_results(TBL_MWARGA);
                    $countBalitaPR = $this->db
                        ->where(COL_KDKELUARGA, $d[COL_KDKELUARGA])
                        ->where("TIMESTAMPDIFF(YEAR,TanggalLahir,CURDATE()) > 0 and TIMESTAMPDIFF(YEAR,TanggalLahir,CURDATE()) <= 4")
                        ->where("EXISTS (select mwargameninggal.NIK from mwargameninggal where mwargameninggal.NIK = mwarga.NIK) = false")
                        ->where(COL_JENISKELAMIN, "Perempuan")
                        ->count_all_results(TBL_MWARGA);
                    $countLansia = $this->db
                        ->where(COL_KDKELUARGA, $d[COL_KDKELUARGA])
                        ->where("TIMESTAMPDIFF(YEAR,TanggalLahir,CURDATE()) > 64")
                        ->where("EXISTS (select mwargameninggal.NIK from mwargameninggal where mwargameninggal.NIK = mwarga.NIK) = false")
                        ->count_all_results(TBL_MWARGA);
                    $countPUS = $this->db
                        ->where(COL_KDKELUARGA, $d[COL_KDKELUARGA])
                        ->where("TIMESTAMPDIFF(YEAR,TanggalLahir,CURDATE()) > 20 and TIMESTAMPDIFF(YEAR,TanggalLahir,CURDATE()) <= 45")
                        ->where("EXISTS (select mwargameninggal.NIK from mwargameninggal where mwargameninggal.NIK = mwarga.NIK) = false")
                        ->where(COL_STATUSKELUARGA, "Suami")
                        ->count_all_results(TBL_MWARGA);
                    $countWUS = $this->db
                        ->where(COL_KDKELUARGA, $d[COL_KDKELUARGA])
                        ->where("TIMESTAMPDIFF(YEAR,TanggalLahir,CURDATE()) > 20 and TIMESTAMPDIFF(YEAR,TanggalLahir,CURDATE()) <= 45")
                        ->where("EXISTS (select mwargameninggal.NIK from mwargameninggal where mwargameninggal.NIK = mwarga.NIK) = false")
                        ->where(COL_JENISKELAMIN, "Perempuan")
                        ->count_all_results(TBL_MWARGA);
                    $countDifabel = $this->db
                        ->where(COL_KDKELUARGA, $d[COL_KDKELUARGA])
                        ->where("EXISTS (select mwargameninggal.NIK from mwargameninggal where mwargameninggal.NIK = mwarga.NIK) = false")
                        ->where(COL_ISDIFABEL, true)
                        ->count_all_results(TBL_MWARGA);

                    $total[3] += $d["COUNT_KK"];
                    $total[4] += $d["COUNT_LK"];
                    $total[5] += $d["COUNT_PR"];
                    $total[6] += $countBalitaLK;
                    $total[7] += $countBalitaPR;
                    $total[8] += $countPUS;
                    $total[9] += $countWUS;
                    $total[10] += 0;
                    $total[11] += 0;
                    $total[12] += $countLansia;
                    $total[13] += 0;
                    $total[14] += $countDifabel;
                    $total[15] += $d[COL_NMKRITERIARUMAH]=="Sehat"?1:0;
                    $total[16] += $d[COL_NMKRITERIARUMAH]=="Kurang Sehat"?1:0;
                    $total[17] += $d[COL_ISPUNYAPEMBUANGANSAMPAH];
                    $total[18] += $d[COL_ISPUNYAPEMBUANGANLIMBAH];
                    $total[19] += $d[COL_ISPUNYAJAMBANKEL];
                    $total[20] += $d[COL_ISTEMPELSTIKERP4K];
                    $total[21] += $d[COL_NMSUMBERAIR]=="PDAM"?1:0;
                    $total[22] += $d[COL_NMSUMBERAIR]=="SUMUR"?1:0;
                    $total[23] += $d[COL_NMSUMBERAIR]=="DLL"?1:0;
                    $total[24] += $d[COL_NMMAKANANPOKOK]=="Beras"?1:0;
                    $total[25] += $d[COL_NMMAKANANPOKOK]=="Non Beras"?1:0;
                    $total[26] += $d[COL_ISAKTIFUP2K]?1:0;
                    $total[27] += $d["COUNT_TANAH"];
                    $total[28] += $d["COUNT_INDUSTRI"];
                    $total[29] += $d["COUNT_KB"];
                    $total[30] += $d["COUNT_LL"];
                    ?>
                    <!--<tr style="display: none">
                        <td style="padding: 5px"><?=$no?>.</td>
                        <td style="padding: 5px"><?=strtoupper($d[COL_NMKEPALAKELUARGA])?></td>
                        <td style="padding: 5px"><?=$d["COUNT_KK"]?></td>
                        <td style="padding: 5px"><?=$d["COUNT_LK"]?></td>
                        <td style="padding: 5px"><?=$d["COUNT_PR"]?></td>
                        <td style="padding: 5px"><?=$countBalitaLK?></td>
                        <td style="padding: 5px"><?=$countBalitaPR?></td>
                        <td style="padding: 5px"><?=$countPUS?></td>
                        <td style="padding: 5px"><?=$countWUS?></td>
                        <td style="padding: 5px">0</td>
                        <td style="padding: 5px">0</td>
                        <td style="padding: 5px"><?=$countLansia?></td>
                        <td style="padding: 5px">0</td>
                        <td style="padding: 5px"><?=$countDifabel?></td>
                        <td style="padding: 5px"><?=$d[COL_NMKRITERIARUMAH]=="Sehat"?1:0?></td>
                        <td style="padding: 5px"><?=$d[COL_NMKRITERIARUMAH]=="Kurang Sehat"?1:0?></td>
                        <td style="padding: 5px"><?=!empty($d[COL_ISPUNYAPEMBUANGANSAMPAH])?1:0?></td>
                        <td style="padding: 5px"><?=!empty($d[COL_ISPUNYAPEMBUANGANLIMBAH])?1:0?></td>
                        <td style="padding: 5px"><?=!empty($d[COL_ISPUNYAJAMBANKEL])?1:0?></td>
                        <td style="padding: 5px"><?=!empty($d[COL_ISTEMPELSTIKERP4K])?1:0?></td>
                        <td style="padding: 5px"><?=$d[COL_NMSUMBERAIR]=="PDAM"?1:0?></td>
                        <td style="padding: 5px"><?=$d[COL_NMSUMBERAIR]=="SUMUR"?1:0?></td>
                        <td style="padding: 5px"><?=$d[COL_NMSUMBERAIR]=="DLL"?1:0?></td>
                        <td style="padding: 5px"><?=$d[COL_NMMAKANANPOKOK]=="Beras"?1:0?></td>
                        <td style="padding: 5px"><?=$d[COL_NMMAKANANPOKOK]=="Non Beras"?1:0?></td>
                        <td style="padding: 5px"><?=$d[COL_ISAKTIFUP2K]?1:0?></td>
                        <td style="padding: 5px"><?=$d["COUNT_TANAH"]?></td>
                        <td style="padding: 5px"><?=$d["COUNT_INDUSTRI"]?></td>
                        <td style="padding: 5px"><?=$d["COUNT_KB"]?></td>
                        <td style="padding: 5px"><?=$d["COUNT_LL"]?></td>
                    </tr>-->
                    <?php
                    $no++;
                }
                $nourut++;
                for($i=0; $i<count($total); $i++) {
                    $gt[$i] += $total[$i];
                }
                ?>
                <tr>
                    <td style="padding: 5px"><?=$nourut?>.</td>
                    <td style="padding: 5px"><?=strtoupper($dat[COL_NMKELOMPOK])?></td>
                    <td style="padding: 5px"><?=$total[3]?></td>
                    <td style="padding: 5px"><?=$total[3]?></td>
                    <td style="padding: 5px"><?=$total[4]?></td>
                    <td style="padding: 5px"><?=$total[5]?></td>
                    <td style="padding: 5px"><?=$total[6]?></td>
                    <td style="padding: 5px"><?=$total[7]?></td>
                    <td style="padding: 5px"><?=$total[8]?></td>
                    <td style="padding: 5px"><?=$total[9]?></td>
                    <td style="padding: 5px"><?=$total[10]?></td>
                    <td style="padding: 5px"><?=$total[11]?></td>
                    <td style="padding: 5px"><?=$total[12]?></td>
                    <td style="padding: 5px"><?=$total[13]?></td>
                    <td style="padding: 5px"><?=$total[14]?></td>
                    <td style="padding: 5px"><?=$total[15]?></td>
                    <td style="padding: 5px"><?=$total[16]?></td>
                    <td style="padding: 5px"><?=$total[17]?></td>
                    <td style="padding: 5px"><?=$total[18]?></td>
                    <td style="padding: 5px"><?=$total[19]?></td>
                    <td style="padding: 5px"><?=$total[20]?></td>
                    <td style="padding: 5px"><?=$total[21]?></td>
                    <td style="padding: 5px"><?=$total[22]?></td>
                    <td style="padding: 5px"><?=$total[23]?></td>
                    <td style="padding: 5px"><?=$total[24]?></td>
                    <td style="padding: 5px"><?=$total[25]?></td>
                    <td style="padding: 5px"><?=$total[26]?></td>
                    <td style="padding: 5px"><?=$total[27]?></td>
                    <td style="padding: 5px"><?=$total[28]?></td>
                    <td style="padding: 5px"><?=$total[29]?></td>
                    <td style="padding: 5px"><?=$total[30]?></td>
                </tr>
                <?php
            }
            ?>
            <tr>
                <td style="padding: 5px" colspan="2">TOTAL</td>
                <td style="padding: 5px"><?=$gt[3]?></td>
                <td style="padding: 5px"><?=$gt[3]?></td>
                <td style="padding: 5px"><?=$gt[4]?></td>
                <td style="padding: 5px"><?=$gt[5]?></td>
                <td style="padding: 5px"><?=$gt[6]?></td>
                <td style="padding: 5px"><?=$gt[7]?></td>
                <td style="padding: 5px"><?=$gt[8]?></td>
                <td style="padding: 5px"><?=$gt[9]?></td>
                <td style="padding: 5px"><?=$gt[10]?></td>
                <td style="padding: 5px"><?=$gt[11]?></td>
                <td style="padding: 5px"><?=$gt[12]?></td>
                <td style="padding: 5px"><?=$gt[13]?></td>
                <td style="padding: 5px"><?=$gt[14]?></td>
                <td style="padding: 5px"><?=$gt[15]?></td>
                <td style="padding: 5px"><?=$gt[16]?></td>
                <td style="padding: 5px"><?=$gt[17]?></td>
                <td style="padding: 5px"><?=$gt[18]?></td>
                <td style="padding: 5px"><?=$gt[19]?></td>
                <td style="padding: 5px"><?=$gt[20]?></td>
                <td style="padding: 5px"><?=$gt[21]?></td>
                <td style="padding: 5px"><?=$gt[22]?></td>
                <td style="padding: 5px"><?=$gt[23]?></td>
                <td style="padding: 5px"><?=$gt[24]?></td>
                <td style="padding: 5px"><?=$gt[25]?></td>
                <td style="padding: 5px"><?=$gt[26]?></td>
                <td style="padding: 5px"><?=$gt[27]?></td>
                <td style="padding: 5px"><?=$gt[28]?></td>
                <td style="padding: 5px"><?=$gt[29]?></td>
                <td style="padding: 5px"><?=$gt[30]?></td>
            </tr>
            <?php
            ?>
            <!--<tr>
                <td style="padding: 5px">No.</td>
                <td style="padding: 5px">Nama Kepala Keluarga</td>
                <td style="padding: 5px">Jlh. KK</td>
                <td style="padding: 5px">(4)</td>
                <td style="padding: 5px">(5)</td>
            </tr>-->
            </tbody>
        </table>
    </div>
</div>