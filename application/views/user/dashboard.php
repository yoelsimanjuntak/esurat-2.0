<?php $this->load->view('header') ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-lg-6 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3 id="ajaxcount-suratmasuk"><i class="fa fa-spinner fa-spin"></i> </h3>

                    <p>Surat Masuk</p>
                </div>
                <div class="icon">
                    <i class="fa fa-inbox"></i>
                </div>
                <a href="<?=site_url("data/suratmasuk")?>" class="small-box-footer">Lihat <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-6 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3 id="ajaxcount-suratkeluar"><i class="fa fa-spinner fa-spin"></i> </h3>

                    <p>Surat Keluar</p>
                </div>
                <div class="icon">
                    <i class="fa fa-share-square"></i>
                </div>
                <a href="<?=site_url("data/suratkeluar")?>" class="small-box-footer">Lihat <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">Surat Diteruskan</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <div class="table-responsive mailbox-messages">
                        <table class="table table-hover table-striped table-mailbox">
                            <tbody id="ajaxload-suratfwd">
                            <tr>
                                <td style='font-style: italic; text-align: center'>Tidak ada data</td>
                            </tr>
                            </tbody>
                        </table>
                        <!-- /.table -->
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <a href="<?=site_url("data/suratfwd")?>" class="btn btn-sm btn-default btn-flat pull-right">Lihat Semua</a>
                </div>
                <!-- /.box-footer -->
            </div>
        </div>
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
<?php $this->load->view('loadjs')?>
<script type="text/javascript">
    $(document).ready(function() {
        $("#ajaxcount-suratmasuk").load("<?=site_url('data/suratmasuk')?>?ajaxcount=1", function() {

        });
        $("#ajaxcount-suratkeluar").load("<?=site_url('data/suratkeluar')?>?ajaxcount=1", function() {

        });

        $("#ajaxload-suratfwd").load("<?=site_url('data/suratfwd')?>?load=1", function() {

        });
    });
</script>
<?php $this->load->view('footer') ?>
