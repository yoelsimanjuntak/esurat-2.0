<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 24/11/2018
 * Time: 20:28
 */
?>

<?php $this->load->view('header')

?>
    <!--<section class="content-header">
        <h1><?= $title ?>  <small>Data</small></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a>
            </li>
            <li class="active">
                Surat Masuk
            </li>
        </ol>
    </section>-->

    <!-- Main content -->
    <section class="content">
        <!--<div class="col-md-3">
            <a href="<?=site_url(array('data', 'suratmasuk-form', MODE_CREATE))?>" class="btn btn-primary btn-block margin-bottom">Buat Baru</a>

            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Status</h3>

                    <div class="box-tools">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body no-padding" style="display: block;">
                    <ul class="nav nav-pills nav-stacked">
                        <li class="active">
                            <a href="#">
                                <i class="fa fa-inbox"></i> Baru
                                <span class="label label-warning pull-right"><?=number_format(count($res), 0)?></span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-share"></i> Diteruskan
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-trash"></i> Dihapus
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>-->
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title"><?=$title?> <small style="font-style: italic; display: none;" id="label-loading">Memuat data...</small></h3>
                    <div class="box-tools" style="max-width: 25%;">
                        <form id="form-search">
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control input-sm" id="txt-search" placeholder="Cari...">
                                <span class="input-group-btn">
                                  <button type="submit" class="btn btn-default btn-flat" id="btn-search"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </form>
                        <!--<input type="text" class="form-control input-sm" placeholder="Cari">-->

                    </div>
                    <!-- /.box-tools -->
                </div>
                <div class="box-body no-padding">
                    <div class="mailbox-controls">
                        <div class="col-md-6" style="text-align: left; padding-left: 0px">
                            <?php
                            if($allowcreate) {
                                ?>
                                <a href="<?=site_url(array('data', $form_url, MODE_CREATE))?>" class="btn btn-primary btn-flat btn-sm">
                                    <i class="fa fa-edit"></i>&nbsp;Buat Baru
                                </a>
                                <?php
                            }
                            ?>

                        </div>
                        <div class="col-md-6" style="text-align: right; padding-right: 0px">
                            <b><?=$offset+1?>-<?=($offset+25)>$countall?$countall:($offset+25)?></b> dari <b><?=number_format($countall, 0)?></b>&nbsp;
                            <div class="btn-group">
                                <a href="<?=($page==1?"javascript:void(0)":(site_url($index_url)."?page=".($page-1)))?>" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></a>
                                <a href="<?=(($offset+25)>$countall?"javascript:void(0)":(site_url($index_url)."?page=".($page+1)))?>" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="table-responsive mailbox-messages">
                        <table class="table table-hover table-striped table-mailbox">
                            <tbody id="tblBody">
                            <tr><td style='font-style: italic; text-align: center'>Tidak ada data</td></tr>
                            </tbody>
                        </table>
                        <!-- /.table -->
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs')?>
<script type="text/javascript">
    function load() {
        var search = $("#txt-search").val();

        $("#label-loading").show();
        $("#tblBody").load("<?=site_url($index_url)?>?load=1&page=<?=$page?>&q="+encodeURIComponent(search), function() {
            $("#label-loading").hide();
        });
    }
    $(document).ready(function() {
        load();
        $("#form-search").submit(function(e) {
            load();
            e.preventDefault();
        });
    });
</script>
<?php $this->load->view('footer') ?>