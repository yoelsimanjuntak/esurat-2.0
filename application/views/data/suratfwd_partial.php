<?php
/**
 * Created by PhpStorm.
 * User: 6217025076
 * Date: 22/12/2018
 * Time: 23:33
 */
?>
<?php
if(count($res) > 0) {
    foreach($res as $d) {
        ?>
        <tr>
            <td style="width: 4%;"><i class="fa fa-<?=$d[COL_KDTIPE]==SURAT_MASUK?"inbox":"share"?>" title="<?=$d[COL_NMTIPE]?>"></i></td>
            <td><small class="label label-success"><?=strtoupper($d[COL_NMSTATUS])?></small></td>

            <td class="mailbox-subject" style="width: 40%" title="<?=$d[COL_NMJUDUL]?> - <?=$d[COL_NMPERIHAL]?>">
                <a href="<?=site_url(array('data', ($d[COL_KDTIPE]==SURAT_MASUK?"suratmasuk":"suratkeluar").'-form', MODE_VIEW, $d[COL_KDSURAT]))?>">
                    <b><?=$d[COL_NMJUDUL]?></b> - <?=strlen($d[COL_NMPERIHAL])>75?substr($d[COL_NMPERIHAL], 0, 75)."...":$d[COL_NMPERIHAL]?>
                </a>
            </td>
            <td class="mailbox-name" style="width: 20%" title="<?=$d[COL_NAME]?>"><?=$d[COL_NAME]?></td>
            <td class="mailbox-date" style="width: 10%; text-align: right; font-style: italic"><?=time_elapsed_string($d["Timestamp"])?></td>
        </tr>
    <?php
    }
} else {
    echo "<tr><td style='font-style: italic; text-align: center'>Tidak ada data</td></tr>";
}
?>