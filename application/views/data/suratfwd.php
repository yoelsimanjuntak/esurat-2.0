<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 24/11/2018
 * Time: 20:28
 */
?>

<?php $this->load->view('header')
?>
    <!--<section class="content-header">
        <h1><?= $title ?>  <small>Data</small></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a>
            </li>
            <li class="active">
                Surat Masuk
            </li>
        </ol>
    </section>-->

    <!-- Main content -->
    <section class="content">
        <!--<div class="col-md-3">
            <a href="<?=site_url(array('data', 'suratmasuk-form', MODE_CREATE))?>" class="btn btn-primary btn-block margin-bottom">Buat Baru</a>

            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Status</h3>

                    <div class="box-tools">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body no-padding" style="display: block;">
                    <ul class="nav nav-pills nav-stacked">
                        <li class="active">
                            <a href="#">
                                <i class="fa fa-inbox"></i> Baru
                                <span class="label label-warning pull-right"><?=number_format(count($res), 0)?></span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-share"></i> Diteruskan
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-trash"></i> Dihapus
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>-->
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title"><?=$title?></h3>
                    <div class="box-tools pull-right">
                        <div class="has-feedback">
                            <input type="text" class="form-control input-sm" placeholder="Cari">
                            <span class="fa fa-search form-control-feedback"></span>
                        </div>
                    </div>
                    <!-- /.box-tools -->
                </div>
                <div class="box-body no-padding">
                    <div class="mailbox-controls">
                        <div class="col-md-12" style="text-align: right; padding-right: 0px">
                            <b>1-50</b> dari <b>200</b>&nbsp;
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="table-responsive mailbox-messages">
                        <table class="table table-hover table-striped table-mailbox">
                            <tbody>

                            </tbody>
                        </table>
                        <!-- /.table -->
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs')?>
<?php $this->load->view('footer') ?>