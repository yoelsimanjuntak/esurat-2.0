<?php
/**
 * Created by PhpStorm.
 * User: 6217025076
 * Date: 22/12/2018
 * Time: 14:37
 */
?>
<?php
if(count($res) > 0) {
    foreach($res as $d) {
        ?>
        <tr>
            <td class="mailbox-name" style="width: 20%" title="<?=$d[COL_NAME]?>"><?=$d[COL_NAME]?></td>
            <td class="mailbox-subject" style="width: 50%" title="<?=$d[COL_NMJUDUL]?> - <?=$d[COL_NMPERIHAL]?>"><b><?=$d[COL_NMJUDUL]?></b> - <?=strlen($d[COL_NMPERIHAL])>75?substr($d[COL_NMPERIHAL], 0, 75)."...":$d[COL_NMPERIHAL]?>
            </td>
            <td class="mailbox-date" style="width: 20%; text-align: right; font-style: italic"><?=time_elapsed_string($d["Timestamp"])?></td>
            <td class="mailbox-date" style="width: 10%; text-align: right">
                <a href="<?=site_url(array('data', $form_url, MODE_EDIT, $d[COL_KDSURAT]))?>" title="Ubah / Tambah Catatan"><i class="fa fa-edit"></i></a>&nbsp;
                <?php
                if($d[COL_KDSTATUS] == 1) {
                    ?>
                    <a href="<?=site_url(array('data', $form_url, MODE_DELETE, $d[COL_KDSURAT]))?>" title="Hapus"><i class="fa fa-trash"></i></a>&nbsp;
                <?php
                }
                ?>

                <a href="<?=site_url(array('data', $form_url, MODE_FORWARD, $d[COL_KDSURAT]))?>" title="Teruskan / Disposisi"><i class="fa fa-share"></i></a>
            </td>
        </tr>
    <?php
    }
} else {
    echo "<tr><td style='font-style: italic; text-align: center'>Tidak ada data</td></tr>";
}
?>