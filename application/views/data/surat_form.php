<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 24/11/2018
 * Time: 21:01
 */
$this->load->view('header') ?>
    <!-- Content Header (Page header) -->
    <!--<section class="content-header">
        <ol class="breadcrumb" style="float: none">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?=site_url('data/suratmasuk')?>"> Surat Masuk</a></li>
            <li class="active"><?=$edit?'Ubah':'Tambah'?></li>
        </ol>
    </section>-->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h4 class="box-title"><?= strtoupper($mode) ." - ". $title ?></h4>
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'surat-add', 'class'=>'form-horizontal'))?>
                        <div class="form-group" style="text-align: right">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-primary btn-flat">Simpan</button>&nbsp;
                                <a href="<?=$redirect?>" class="btn btn-default btn-flat">Kembali</a>
                            </div>
                        </div>

                        <div style="display: none" class="alert alert-danger errorBox">
                            <i class="fa fa-ban"></i>
                            <span class="errorMsg"></span>
                        </div>
                        <?php
                        if($this->input->get('success') == 1){
                            ?>
                            <div class="alert alert-success">
                                <i class="fa fa-check"></i> Error Detail :
                                <span class="">Data disimpan</span>
                            </div>
                        <?php
                        }
                        ?>

                        <?php
                        if($mode == MODE_FORWARD) {
                            ?>
                            <h4 style="text-decoration: underline">Disposisi / Distribusi</h4>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Status</label>
                                <div class="col-sm-8">
                                    <select name="<?=COL_KDSTATUS?>" class="form-control" required>
                                        <?=GetCombobox("SELECT * FROM mstatus_opsi INNER JOIN mstatus on mstatus.KdStatus = mstatus_opsi.KdOpsi where mstatus_opsi.KdStatus = ".$data[COL_KDSTATUS]." and (KdRole is null or KdRole = ".$ruser[COL_ROLEID].") ORDER BY mstatus_opsi.KdOpsi", COL_KDSTATUS, COL_NMSTATUS)?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">OPD / Instansi</label>
                                <div class="col-sm-8">
                                    <select name="KdOPD[]" class="form-control" multiple>
                                        <?=GetCombobox("SELECT * FROM mopd ORDER BY NmOPD", COL_KDOPD, COL_NMOPD)?>
                                    </select>
                                </div>
                            </div>
                            <hr />
                            <?php
                        }
                        ?>

                        <div class="form-group">
                            <label class="control-label col-sm-2">Kop / No. Surat</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="<?=COL_NMJUDUL?>" value="<?= $edit ? strip_tags($data[COL_NMJUDUL]) : ""?>" maxlength="200" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">Perihal</label>
                            <div class="col-sm-8">
                                <textarea class="form-control" rows="3" name="<?=COL_NMPERIHAL?>"><?=!empty($data[COL_NMPERIHAL]) ? $data[COL_NMPERIHAL] : ''?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">File</label>
                            <div class="col-sm-8">
                                <?php
                                if($mode != MODE_CREATE) {
                                    ?>
                                    <object id="preview-data" data="<?=MY_UPLOADURL.$data[COL_NMFILE]?>" type="<?=mime_content_type(MY_UPLOADPATH.$data[COL_NMFILE])?>" width="100%" height="400">
                                    </object>
                                    <?php
                                }
                                ?>
                                <input name="userfile" type="file" />
                            </div>
                        </div>
                        <?php
                        if($mode != MODE_CREATE) {
                            $this->db->join(TBL_USERINFORMATION,TBL_USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TSURAT_KOMENTAR.".".COL_CREATEDBY,"inner");
                            $this->db->join(TBL_TSURAT,TBL_TSURAT.".".COL_KDSURAT." = ".TBL_TSURAT_KOMENTAR.".".COL_KDSURAT,"inner");
                            $this->db->join(TBL_TSURAT." t2",'t2.'.COL_KDSURAT." = ".TBL_TSURAT.".".COL_KDSUMBER,"left");
                            $this->db->where("((tsurat_komentar.KdSurat = ".($data[COL_KDSUMBER]?$data[COL_KDSUMBER]:-1)." or tsurat_komentar.KdSurat = ".$data[COL_KDSURAT].") and (userinformation.CompanyID is null or tsurat.KdTipe = ".SURAT_KELUAR."))");
                            if(!empty($ruser[COL_COMPANYID])) {
                                $this->db->or_where("(tsurat_komentar.KdSurat = ".$data[COL_KDSURAT]." and userinformation.CompanyID = ".$ruser[COL_COMPANYID].")");
                            }
                            $this->db->order_by(TBL_TSURAT_KOMENTAR.".".COL_CREATEDAT, 'desc');
                            $query_comment = $this->db->get(TBL_TSURAT_KOMENTAR);

                            $this->db->join(TBL_USERINFORMATION,TBL_USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TSURAT_LOGSTAT.".".COL_CREATEDBY,"inner");
                            $this->db->join(TBL_TSURAT,TBL_TSURAT.".".COL_KDSURAT." = ".TBL_TSURAT_LOGSTAT.".".COL_KDSURAT,"inner");
                            $this->db->join(TBL_TSURAT." t2",'t2.'.COL_KDSURAT." = ".TBL_TSURAT.".".COL_KDSUMBER,"left");
                            $this->db->where("((tsurat_logstat.KdSurat = ".($data[COL_KDSUMBER]?$data[COL_KDSUMBER]:-1)." or tsurat_logstat.KdSurat = ".$data[COL_KDSURAT].") and (userinformation.CompanyID is null or tsurat.KdTipe = ".SURAT_KELUAR."))");
                            if(!empty($ruser[COL_COMPANYID])) {
                                $this->db->or_where("(tsurat_logstat.KdSurat = ".$data[COL_KDSURAT]." and userinformation.CompanyID = ".$ruser[COL_COMPANYID].")");
                            }
                            $this->db->where(TBL_TSURAT_LOGSTAT.".".COL_KDSTATUS." != ", "1");
                            $this->db->where(TBL_TSURAT_LOGSTAT.".".COL_KDSTATUS." != ", "26");
                            $this->db->order_by(TBL_TSURAT_LOGSTAT.".".COL_CREATEDAT, 'desc');
                            $query_log = $this->db->get(TBL_TSURAT_LOGSTAT);
                            //echo $this->db->last_query();
                            ?>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Log</label>
                                <div class="col-sm-8">
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <td>Dibuat</td>
                                            <td width="10px">:</td>
                                            <td><b><?=date('d/m/Y H:i', strtotime($data[COL_CREATEDAT]))?></b> oleh <b><?=$data[COL_NAME]?></b></td>
                                        </tr>
                                        <?php
                                        if(!empty($data[COL_MODIFIEDAT])) {
                                            ?>
                                            <tr>
                                                <td>Terakhir diubah</td>
                                                <td width="10px">:</td>
                                                <td><b><?=date('d/m/Y H:i', strtotime($data[COL_MODIFIEDAT]))?></b> oleh <b><?=$data["MName"]?></b></td>
                                            </tr>
                                        <?php
                                        }
                                        $lastcomment = $query_comment->row_array();
                                        $lastfwd = $query_log->row_array();
                                        if($lastcomment) {
                                            ?>
                                            <tr>
                                                <td>Terakhir dikomentari</td>
                                                <td width="10px">:</td>
                                                <td><b><?=date('d/m/Y H:i', strtotime($lastcomment[COL_CREATEDAT]))?></b> oleh <b><?=$lastcomment[COL_NAME]?></b></td>
                                            </tr>
                                        <?php
                                        }
                                        if($lastfwd) {
                                            ?>
                                            <tr>
                                                <td>Terakhir diteruskan</td>
                                                <td width="10px">:</td>
                                                <td><b><?=date('d/m/Y H:i', strtotime($lastfwd[COL_CREATEDAT]))?></b> oleh <b><?=$lastfwd[COL_NAME]?></b></td>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        <?=form_close()?>
                    </div>
                </div>

                <?php
                if($mode != MODE_CREATE) {
                    ?>
                    <div class="box box-warning" id="box-comment">
                        <li class="comment-blueprint" style="display: none">
                            <i class="fa fa-sticky-note bg-yellow comment-icon"></i>
                            <div class="timeline-item">
                                <span class="time"><i class="fa fa-clock-o"></i>&nbsp; baru saja</span>
                                <h3 class="timeline-header"><b><?=$ruser[COL_NAME]?></b></h3>

                                <div class="timeline-body">
                                    <p style="text-align: justify">
                                    </p>
                                </div>
                            </div>
                        </li>
                        <div class="box-header with-border">
                            <h4 class="box-title">Komentar / Catatan</h4>
                            <div class="box-tools">
                                <button type="button" class="btn btn-box-tool" onclick="refresh_comments()"><i class="fa fa-refresh"></i></button>
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <?php
                        if($allowcomment) {
                            ?>
                            <div class="box-body">
                                <?=form_open_multipart(site_url(array("data", "komentar", $data[COL_KDSURAT])),array('role'=>'form','id'=>'surat-comment', 'class'=>'form-horizontal'))?>
                                <div style="display: none" class="alert alert-danger errorBox">
                                    <i class="fa fa-ban"></i> Error Detail : <br />
                                    <span class="errorMsg"></span>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-2">Catatan</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control" rows="3" name="<?=COL_NMKOMENTAR?>" required="required"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Lampiran</label>
                                    <div class="col-sm-8">
                                        <input name="userfile" type="file" />
                                    </div>
                                </div>
                                <div class="form-group" style="text-align: right">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-sm btn-primary btn-flat">Tambah</button>
                                    </div>
                                </div>
                                <?=form_close()?>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="box-footer" style="padding-top: 0px">

                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs') ?>
    <script type="text/javascript">
        var mode = '<?=$mode?>';
        var status = '<?=$data?$data[COL_KDSTATUS]:1?>';
        if((mode != '<?=MODE_CREATE?>' && mode != '<?=MODE_EDIT?>') || status != 1) {
            $("input, select, textarea", $("#surat-add")).attr("readonly", true);
            $("input[type=file]", $("#surat-add")).attr("disabled", true);
        }
        if(mode == '<?=MODE_VIEW?>' || status != 1) {
            $("button[type=submit]", $("#surat-add")).attr("disabled", true).hide();
        }
        if(mode == '<?=MODE_DELETE?>') {
            $("button[type=submit]", $("#surat-add")).removeClass("btn-primary").addClass("btn-danger").html("Hapus");
        }
        if(mode == '<?=MODE_FORWARD?>') {
            $("button[type=submit]", $("#surat-add")).attr("disabled", false).show();
            $("[name=<?=COL_KDSTATUS?>]").attr("disabled", false);
        }
        $("#surat-add").validate({
            submitHandler : function(form){
                $(form).find('button[type=submit]').attr('disabled',true);
                $(form).ajaxSubmit({
                    dataType: 'json',
                    type : 'post',
                    success : function(data){
                        $(form).find('btn').attr('disabled',false);
                        if(data.error != 0){
                            $('.errorBox', $(form)).show().find('.errorMsg').text(data.error);
                        }else{
                            window.location.href = data.redirect;
                        }
                    },
                    error : function(a,b,c){
                        console.log(a);
                        //alert('Response Error');
                        $('.errorBox', $(form)).show().find('.errorMsg').html(a.responseText);
                    },
                    complete : function() {
                        $(form).find('button[type=submit]').attr('disabled',false);
                    }
                });
                return false;
            }
        });
        $("#surat-comment").validate({
            submitHandler : function(form){
                $(form).find('button[type=submit]').attr('disabled',true);
                $(form).ajaxSubmit({
                    dataType: 'json',
                    type : 'post',
                    success : function(data){
                        $(form).find('btn').attr('disabled',false);
                        if(data.error != 0){
                            $('.errorBox', $(form)).show().find('.errorMsg').text(data.error);
                        }else{
                            //location.reload();
                            form.reset();
                            append_comment(form, function() {
                                refresh_comments(false);
                            });
                        }
                    },
                    error : function(a,b,c){
                        console.log(a);
                        //console.log((JSON.parse(a.responseText)).error);
                        //alert('Response Error');
                        $('.errorBox', $(form)).show().find('.errorMsg').html(a.responseText);
                    },
                    complete : function() {
                        $(form).find('button[type=submit]').attr('disabled',false);
                    }
                });
                return false;
            }
        });

        function append_comment(form, cb) {
            var blueprint = $(".comment-blueprint", $("#box-comment")).clone();
            var comfoot = $(".box-footer", $("#box-comment"));

            blueprint.removeClass(".comment-blueprint");
            blueprint.prependTo(comfoot.find(".timeline")).fadeIn("slow", function() {
                (cb)();
            });
        }

        function refresh_comments(fade=true) {
            var comfoot = $(".box-footer", $("#box-comment"));
            if(fade) comfoot.fadeOut("slow");
            comfoot.load("<?=site_url(array("data", "komentar-load", $data[COL_KDSURAT]))?>", function(){
                if(fade) comfoot.fadeIn("slow");
            });
        }

        $(document).ready(function() {
            refresh_comments();

            var form = $("#surat-add");
            $("[name=<?=COL_KDSTATUS?>]", form).change(function() {
                $.ajax({url: "<?=site_url(array("ajax", "check-status"))?>?id="+$(this).val(), success: function(result){
                    if(result == 1) {
                        $("[name^=<?=COL_KDOPD?>]", form).closest(".form-group").show();
                        $("[name^=<?=COL_KDOPD?>]", form).attr("required", true);
                    }
                    else {
                        $("[name^=<?=COL_KDOPD?>]", form).closest(".form-group").hide();
                        $("[name^=<?=COL_KDOPD?>]", form).attr("required", false);
                    }
                }});
            }).trigger("change");
        });
    </script>
<?php $this->load->view('footer') ?>