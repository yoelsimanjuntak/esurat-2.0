<?php
/**
 * Created by PhpStorm.
 * User: 6217025076
 * Date: 02/12/2018
 * Time: 13:57
 */
if(count($comments) > 0) {
    ?>
    <ul class="timeline timeline-inverse">
        <li class="time-label"></li>
        <?php
        foreach($comments as $c) {
            if($c["Type"] == "Log") {
                ?>
                <li>
                    <i class="fa <?=$c[COL_KDSTATUS]==1?"fa-plus":"fa-share"?> bg-green"></i>
                    <div class="timeline-item">
                        <span class="time"><i class="fa fa-clock-o"></i>&nbsp;<?= time_elapsed_string($c[COL_CREATEDAT]) ?></span>
                        <h3 class="timeline-header no-border">
                            <?php
                            if($c[COL_KDSTATUS] == 1 || $c[COL_KDSTATUS] == 26) {
                                ?>
                                Data diinput oleh <b><?=$c[COL_NAME]?></b>.
                            <?php
                            } else {
                                ?>
                                <b><?=$c[COL_NAME]?></b> mengubah status menjadi <u><?=$c[COL_NMKOMENTAR]?></u>.
                                <?php
                            }
                            ?>
                        </h3>
                    </div>
                </li>
                <?php

            } else {
                ?>
                <li>
                    <i class="fa fa-<?= empty($c[COL_NMFILE]) ? "sticky-note" : "paperclip" ?> bg-yellow"></i>

                    <div class="timeline-item">
                        <span class="time"><i
                                class="fa fa-clock-o"></i>&nbsp;<?= time_elapsed_string($c[COL_CREATEDAT]) ?></span>

                        <h3 class="timeline-header"><b><?= $c[COL_NAME] ?></b></h3>

                        <div class="timeline-body">
                            <p style="text-align: justify">
                                <?= $c[COL_NMKOMENTAR] ?>
                            </p>
                        </div>

                        <?php
                        if (!empty($c[COL_NMFILE])) {
                            ?>
                            <div class="timeline-footer" style="border-top: 1px solid #ddd; text-align: right">
                                <a href="<?= MY_UPLOADURL . $c[COL_NMFILE] ?>" target="_blank"><i
                                        class="fa fa-paperclip"></i>&nbsp;Lihat lampiran</a>
                            </div>
                        <?php
                        }
                        ?>

                    </div>
                </li>
            <?php
            }
        }
        ?>
        <li>
            <i class="fa fa-clock-o bg-gray"></i>
        </li>
    </ul>
<?php
}
else {
    ?>
    <p style="font-style: italic">Belum ada data</p>
    <?php
}
?>