<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    public function index()
    {
        /*$lang = $this->input->post(COL_LANGUAGEID);
        $keyword = $this->input->post("Keyword");

        $this->db->select(TBL_TRANSLATION.".*, ".TBL_LANGUAGE.".".COL_LANGUAGENAME." as ".COL_LANGUAGENAME);
        $this->db->join(TBL_LANGUAGE,TBL_LANGUAGE.'.'.COL_LANGUAGEID." = ".TBL_TRANSLATION.".".COL_LANGUAGEID,"inner");

        if(!empty($keyword)) {
            $this->db->like(TBL_TRANSLATION.'.'.COL_WORD, $keyword);
            if(!empty($lang)) $this->db->where(TBL_LANGUAGE.'.'.COL_LANGUAGEID, $lang);
        }
        else {
            $this->db->where(TBL_LANGUAGE.'.'.COL_LANGUAGEID, -999);
        }

        $this->db->order_by(COL_WORD, 'asc');
        $data['res'] = $this->db->get(TBL_TRANSLATION)->result_array();

        $this->load->view('home/index', $data);*/
        redirect('user/dashboard');
        if(IsLogin()) {
            //redirect('user/dashboard');
        }
        /*$data['title'] = 'Dasawisma';
        $this->db->select('*, (SELECT COUNT(*) FROM mkeluarga kk WHERE kk.KdDasawisma=mdasawisma.KdDasawisma) AS COUNT_KK');
        $this->db->join(TBL_MDUSUN,TBL_MDUSUN.'.'.COL_KDDUSUN." = ".TBL_MDASAWISMA.".".COL_KDDUSUN,"inner");
        $this->db->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_KDKELURAHAN." = ".TBL_MDUSUN.".".COL_KDKELURAHAN,"inner");
        $this->db->order_by(COL_NMKELOMPOK, 'asc');
        $data['res'] = $this->db->get(TBL_MDASAWISMA)->result_array();*/

        //$this->load->view('home/index');
    }

    function _404() {
        $this->load->view('home/error');
    }
}
