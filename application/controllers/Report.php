<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 03/10/2018
 * Time: 20:17
 */
class Report extends MY_Controller {
    function __construct() {
        parent::__construct();
        if(!IsLogin() || (GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLEUSER)) {
            redirect('user/dashboard');
        }
    }

    function rekap_kk() {
        //load mPDF library
        $this->load->library('Mypdf');
        $pdf = new Mypdf("", "A4-L");

        //$this->db->select('*, (SELECT COUNT(*) FROM mwarga w WHERE w.KdKeluarga=mkeluarga.KdKeluarga) AS COUNT_ANGGOTA, (SELECT COUNT(*) FROM mwarga w WHERE w.KdKeluarga=mkeluarga.KdKeluarga and w.Jeniskelamin=\'Laki-Laki\') AS COUNT_LK, (SELECT COUNT(*) FROM mwarga w WHERE w.KdKeluarga=mkeluarga.KdKeluarga and w.Jeniskelamin=\'Perempuan\') AS COUNT_PR, (SELECT COUNT(*) FROM mwarga w WHERE w.KdKeluarga=mkeluarga.KdKeluarga and w.StatusKeluarga=\'Suami\') AS COUNT_KK');
        //$this->db->join(TBL_MDASAWISMA,TBL_MDASAWISMA.'.'.COL_KDDASAWISMA." = ".TBL_MKELUARGA.".".COL_KDDASAWISMA,"inner");
        $this->db->join(TBL_MDUSUN,TBL_MDUSUN.'.'.COL_KDDUSUN." = ".TBL_MDASAWISMA.".".COL_KDDUSUN,"inner");
        $this->db->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_KDKELURAHAN." = ".TBL_MDUSUN.".".COL_KDKELURAHAN,"inner");
        $this->db->join(TBL_MKECAMATAN,TBL_MKECAMATAN.'.'.COL_KDKECAMATAN." = ".TBL_MKELURAHAN.".".COL_KDKECAMATAN,"inner");
        $this->db->join(TBL_MKABUPATEN,TBL_MKABUPATEN.'.'.COL_KDKABUPATEN." = ".TBL_MKECAMATAN.".".COL_KDKABUPATEN,"inner");
        $this->db->join(TBL_MPROVINSI,TBL_MPROVINSI.'.'.COL_KDPROVINSI." = ".TBL_MKABUPATEN.".".COL_KDPROVINSI,"inner");
        $data['res'] = $this->db->get(TBL_MDASAWISMA)->result_array();

        //generate the PDF from the given html
        $html = $this->load->view('report/rekap_kk',$data,true);
        //echo $html;
        //return;
        $pdf->pdf->SetHTMLFooter("<p style='font-style: italic; font-size: 10px'>Dicetak melalui aplikasi SIDAMA pada ".date("d/m/Y H:i:s")."</p>");
        $pdf->pdf->WriteHTML($html);

        //download it.
        $pdf->pdf->Output("SIDAMA - Rekapitulasi Kelompok Dasawisma ".date("d-m-Y").".pdf", "I");
        //echo $html;
    }

    function rekap_kel() {
        //load mPDF library
        $this->load->library('Mypdf');
        $pdf = new Mypdf("", "A4-L");

        //$this->db->select('*, (SELECT COUNT(*) FROM mwarga w WHERE w.KdKeluarga=mkeluarga.KdKeluarga) AS COUNT_ANGGOTA, (SELECT COUNT(*) FROM mwarga w WHERE w.KdKeluarga=mkeluarga.KdKeluarga and w.Jeniskelamin=\'Laki-Laki\') AS COUNT_LK, (SELECT COUNT(*) FROM mwarga w WHERE w.KdKeluarga=mkeluarga.KdKeluarga and w.Jeniskelamin=\'Perempuan\') AS COUNT_PR, (SELECT COUNT(*) FROM mwarga w WHERE w.KdKeluarga=mkeluarga.KdKeluarga and w.StatusKeluarga=\'Suami\') AS COUNT_KK');
        //$this->db->join(TBL_MDASAWISMA,TBL_MDASAWISMA.'.'.COL_KDDASAWISMA." = ".TBL_MKELUARGA.".".COL_KDDASAWISMA,"inner");
        $this->db->join(TBL_MDUSUN,TBL_MDUSUN.'.'.COL_KDDUSUN." = ".TBL_MDASAWISMA.".".COL_KDDUSUN,"inner");
        $this->db->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_KDKELURAHAN." = ".TBL_MDUSUN.".".COL_KDKELURAHAN,"inner");
        $this->db->join(TBL_MKECAMATAN,TBL_MKECAMATAN.'.'.COL_KDKECAMATAN." = ".TBL_MKELURAHAN.".".COL_KDKECAMATAN,"inner");
        $this->db->join(TBL_MKABUPATEN,TBL_MKABUPATEN.'.'.COL_KDKABUPATEN." = ".TBL_MKECAMATAN.".".COL_KDKABUPATEN,"inner");
        $this->db->join(TBL_MPROVINSI,TBL_MPROVINSI.'.'.COL_KDPROVINSI." = ".TBL_MKABUPATEN.".".COL_KDPROVINSI,"inner");
        $data['res'] = $this->db->get(TBL_MDASAWISMA)->result_array();

        //generate the PDF from the given html
        $html = $this->load->view('report/rekap_kel',$data,true);
        //echo $html;
        //return;
        $pdf->pdf->SetHTMLFooter("<p style='font-style: italic; font-size: 10px'>Dicetak melalui aplikasi SIDAMA pada ".date("d/m/Y H:i:s")."</p>");
        $pdf->pdf->WriteHTML($html);

        //download it.
        $pdf->pdf->Output("SIDAMA - Rekapitulasi Kelompok Dasawisma ".date("d-m-Y").".pdf", "I");
        //echo $html;
    }
}