<?php
class Master extends MY_Controller {
    function __construct() {
        parent::__construct();
        if(!IsLogin() || GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }
    }

    function language() {
        $data['title'] = 'Bahasa';
        $this->db->order_by(COL_LANGUAGENAME, 'asc');
        $data['res'] = $this->db->get(TBL_LANGUAGE)->result_array();
        $this->load->view('master/language', $data);
    }
    function langadd() {
        $data['title'] = "Bahasa";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('master/language');
            $data = array(
                COL_LANGUAGENAME => $this->input->post(COL_LANGUAGENAME)
            );
            if(!$this->db->insert(TBL_LANGUAGE, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('master/languageform',$data);
        }
    }
    function langedit($id) {
        $rdata = $data['data'] = $this->db->where(COL_LANGUAGEID, $id)->get(TBL_LANGUAGE)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['title'] = 'Bahasa';
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('master/language');
            $data = array(
                COL_LANGUAGENAME => $this->input->post(COL_LANGUAGENAME)
            );
            if(!$this->db->where(COL_LANGUAGEID, $id)->update(TBL_LANGUAGE, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('master/languageform',$data);
        }
    }

    function translation() {
        $data['title'] = 'Transalasi';
        $this->db->select(TBL_TRANSLATION.".*, ".TBL_LANGUAGE.".".COL_LANGUAGENAME." as ".COL_LANGUAGENAME);
        $this->db->join(TBL_LANGUAGE,TBL_LANGUAGE.'.'.COL_LANGUAGEID." = ".TBL_TRANSLATION.".".COL_LANGUAGEID,"inner");
        $this->db->order_by(COL_WORD, 'asc');
        $data['res'] = $this->db->get(TBL_TRANSLATION)->result_array();
        $this->load->view('master/translation', $data);
    }
    function transadd() {
        $data['title'] = "Translasi";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('master/translation');
            $data = array(
                COL_LANGUAGEID => $this->input->post(COL_LANGUAGEID),
                COL_WORD => $this->input->post(COL_WORD),
                COL_TRANSLATION => $this->input->post(COL_TRANSLATION)
            );
            if(!$this->db->insert(TBL_TRANSLATION, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('master/translationform',$data);
        }
    }
    function transedit($id) {
        $rdata = $data['data'] = $this->db->where(COL_TRANSLATIONID, $id)->get(TBL_TRANSLATION)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['title'] = 'Translasi';
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('master/translation');
            $data = array(
                COL_LANGUAGEID => $this->input->post(COL_LANGUAGEID),
                COL_WORD => $this->input->post(COL_WORD),
                COL_TRANSLATION => $this->input->post(COL_TRANSLATION)
            );
            if(!$this->db->where(COL_TRANSLATIONID, $id)->update(TBL_TRANSLATION, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('master/translationform',$data);
        }
    }

    function opd() {
        $data['title'] = 'OPD';
        $this->db->order_by(COL_NMOPD, 'asc');
        $data['res'] = $this->db->get(TBL_MOPD)->result_array();
        $this->load->view('master/opd', $data);
    }

    function opd_add() {
        $data['title'] = "OPD";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('master/opd');
            $data = array(
                COL_NMOPD => $this->input->post(COL_NMOPD)
            );
            if(!$this->db->insert(TBL_MOPD, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('master/opdform',$data);
        }
    }

    function opd_edit($id) {
        $rdata = $data['data'] = $this->db->where(COL_KDOPD, $id)->get(TBL_MOPD)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }

        $data['title'] = 'OPD';
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('master/opd');
            $data = array(
                COL_NMOPD => $this->input->post(COL_NMOPD)
            );
            if(!$this->db->where(COL_KDOPD, $id)->update(TBL_MOPD, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('master/opdform',$data);
        }
    }
}