<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 24/11/2018
 * Time: 20:25
 */
class Data extends MY_Controller {
    function __construct() {
        parent::__construct();
        if(!IsLogin()) {
            redirect('user/dashboard');
        }
    }

    function suratmasuk() {
        $ruser = GetLoggedUser();
        $data['title'] = 'Surat Masuk';
        $data['form_url'] = 'suratmasuk-form';
        $data['index_url'] = 'data/suratmasuk';
        $data['allowcreate'] = in_array($ruser[COL_ROLEID], allowCreateSuratMasuk());

        $isload = $this->input->get("load");
        $isAjaxCount = $this->input->get("ajaxcount");
        $page = $this->input->get("page") ? $this->input->get("page") : 1;
        $q = $this->input->get("q");
        $offset = ($page-1)*25;

        $data['page'] = $page;
        $data['offset'] = $offset;

        $query = $this->db;
        if($ruser[COL_ROLEID] != ROLEADMIN) {
            $allowed_roles =  $this->db->select(COL_KDSTATUS)->where(COL_KDROLE, $ruser[COL_ROLEID])->from(TBL_MSTATUS_ROLE)->get_compiled_select();
            $query->where(TBL_TSURAT.".".COL_KDSTATUS." IN ($allowed_roles)", NULL, FALSE);
        }
        $query->select("tsurat.*, userinformation.Name, (select CreatedAt from tsurat_logstat inner join userinformation i on i.UserName = tsurat_logstat.CreatedBy where (tsurat_logstat.KdSurat = tsurat.KdSurat or tsurat_logstat.KdSurat = tsurat.KDSumber) and (tsurat.KdSumber is null or i.CompanyID is null or i.CompanyID = t2.KdOPD or ".(!empty($ruser[COL_COMPANYID])?"i.CompanyID = '".$ruser[COL_COMPANYID]."'":"1=1").") order by tsurat_logstat.CreatedAt desc limit 1) as Timestamp");
        $query->join(TBL_TSURAT." t2",'t2.'.COL_KDSURAT." = ".TBL_TSURAT.".".COL_KDSUMBER,"left");
        $query->join(TBL_USERINFORMATION,TBL_USERINFORMATION.'.'.COL_USERNAME." = (select tsurat_logstat.CreatedBy from tsurat_logstat inner join userinformation i on i.UserName = tsurat_logstat.CreatedBy where (tsurat_logstat.KdSurat = tsurat.KdSurat or tsurat_logstat.KdSurat = tsurat.KDSumber) and (i.CompanyID is null or i.CompanyID = t2.KdOPD or ".(!empty($ruser[COL_COMPANYID])?"i.CompanyID = '".$ruser[COL_COMPANYID]."'":"1=1").") order by tsurat_logstat.CreatedAt desc limit 1)","inner");
        if(!empty($ruser[COL_COMPANYID])) {
            $query->where(TBL_TSURAT.".".COL_KDOPD, $ruser[COL_COMPANYID]);
        }
        else {
            $query->where(TBL_TSURAT.".".COL_KDOPD, null);
        }
        $query->where(TBL_TSURAT.".".COL_KDTIPE, SURAT_MASUK);
        $query->where(TBL_TSURAT.".".COL_DELETED, null);
        if(!empty($q)) {
            $query->where("(tsurat.NmJudul like '%$q%' or tsurat.NmPerihal like '%$q%' or userinformation.Name like '%$q%')");
        }
        $query->order_by(TBL_TSURAT.".".COL_FORWARDEDAT, 'desc');
        $query->order_by(TBL_TSURAT.".".COL_CREATEDAT, 'desc');
        if($isload) {

            $data['res'] = $query->get(TBL_TSURAT, $offset, 25)->result_array();
            //echo $this->db->last->query();
            //return;
            $this->load->view('data/surat_partial', $data);
        }
        else if($isAjaxCount) {
            $count = $query->get(TBL_TSURAT)->num_rows();
            echo number_format($count, 0);
            return;
        }
        else {
            $data['countall'] = $query->count_all_results(TBL_TSURAT);
            $this->load->view('data/surat', $data);
        }
    }

    function suratmasuk_form($mode, $id=0) {
        $ruser = GetLoggedUser();

        if($mode == MODE_CREATE/* || $mode == MODE_EDIT*/ || $mode == MODE_DELETE) {
            if(!in_array($ruser[COL_ROLEID], allowCreateSuratMasuk())) {
                show_404();
                return;
            }
        }
        $data['title'] = "Surat Masuk";
        $data['edit'] = $mode != MODE_CREATE;
        $data['mode'] = $mode;
        $data['tipe'] = SURAT_MASUK;
        $data['redirect'] = site_url('data/suratmasuk');
        $data['allowcomment'] = true;
        $data['ruser'] = $ruser;

        $rdata = $data['data'] = null;
        if($mode != MODE_CREATE) {
            $rdata = $data['data'] = $this->db
                ->select("tsurat.*, userinformation.Name, u2.Name as MName")
                ->join(TBL_USERINFORMATION,TBL_USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TSURAT.".".COL_CREATEDBY,"inner")
                ->join(TBL_USERINFORMATION." u2",'u2.'.COL_USERNAME." = ".TBL_TSURAT.".".COL_CREATEDBY,"inner")
                ->where(COL_KDSURAT, $id)->get(TBL_TSURAT)->row_array();
            if(empty($rdata)){
                show_404();
                return;
            }

            /* Jika sudah di disposisi tidak bisa delete */
            if($mode == MODE_DELETE && $rdata[COL_KDSTATUS] != 1) {
                show_404();
                return;
            }

            /* Jika status sudah tidak sesuai role tidak bisa comment */
            $role =  $this->db
                ->select(COL_KDSTATUS)
                ->where(array(COL_KDROLE=>$ruser[COL_ROLEID], COL_KDSTATUS=>$rdata[COL_KDSTATUS]))
                ->get(TBL_MSTATUS_ROLE)
                ->row_array();
            if(empty($role) && $ruser[COL_ROLEID] != ROLEADMIN) {
                $data['allowcomment'] = false;
            }
        }


        if(!empty($_POST)){
            $config['upload_path'] = MY_UPLOADPATH;
            $config['allowed_types'] = UPLOAD_ALLOWEDTYPES;
            $config['max_size']	= 512000;
            $config['max_width']  = 1600;
            $config['max_height']  = 1600;
            $config['overwrite'] = FALSE;

            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = $data['redirect'];
            $data = array(
                COL_KDTIPE => SURAT_MASUK,
                COL_KDSTATUS => SURAT_STAT_BARU
            );

            $this->load->library('upload', $config);
            if (!empty($_FILES["userfile"]["name"])) {
                if (!$this->upload->do_upload()) {
                    $resp['error'] = $this->upload->display_errors();
                    $resp['success'] = 0;
                    echo json_encode($resp);
                    return;
                }

                $dataupload = $this->upload->data();
                if (!empty($dataupload) && $dataupload['file_name']) {
                    $data[COL_NMFILE] = $dataupload['file_name'];
                }
            }

            $this->db->trans_begin();
            if($mode == MODE_CREATE) {
                $data[COL_NMJUDUL] = $this->input->post(COL_NMJUDUL);
                $data[COL_NMPERIHAL] = $this->input->post(COL_NMPERIHAL);
                $data[COL_CREATEDAT] = date("Y-m-d H:i:s");
                $data[COL_CREATEDBY] = $ruser[COL_USERNAME];

                if (!$this->db->insert(TBL_TSURAT, $data)) {
                    $resp['error'] = 1;
                    $resp['success'] = 0;
                    $this->db->trans_rollback();
                    echo json_encode($resp);
                    return;
                }

                $datalog = array(
                    COL_KDSURAT => $this->db->insert_id(),
                    COL_KDSTATUS => $data[COL_KDSTATUS],
                    COL_CREATEDAT => date("Y-m-d H:i:s"),
                    COL_CREATEDBY => $ruser[COL_USERNAME]
                );

                if (!$this->db->insert(TBL_TSURAT_LOGSTAT, $datalog)) {
                    $resp['error'] = 1;
                    $resp['success'] = 0;
                    $this->db->trans_rollback();
                    echo json_encode($resp);
                    return;
                }
            }
            else if($mode == MODE_EDIT) {
                $data[COL_NMJUDUL] = $this->input->post(COL_NMJUDUL);
                $data[COL_NMPERIHAL] = $this->input->post(COL_NMPERIHAL);
                $data[COL_MODIFIEDAT] = date("Y-m-d H:i:s");
                $data[COL_MODIFIEDBY] = $ruser[COL_USERNAME];

                if(!$this->db->where(COL_KDSURAT, $id)->update(TBL_TSURAT, $data)){
                    $resp['error'] = 1;
                    $resp['success'] = 0;
                    $this->db->trans_rollback();
                    echo json_encode($resp);
                    return;
                }
            }
            else if($mode == MODE_DELETE) {
                $data[COL_DELETED] = true;
                $data[COL_DELETEDAT] = date("Y-m-d H:i:s");
                $data[COL_DELETEDBY] = $ruser[COL_USERNAME];

                if(!$this->db->where(COL_KDSURAT, $id)->update(TBL_TSURAT, $data)){
                    $resp['error'] = 1;
                    $resp['success'] = 0;
                    $this->db->trans_rollback();
                    echo json_encode($resp);
                    return;
                }
            }
            else if($mode == MODE_FORWARD) {
                $resp['redirect'] = site_url('data/suratmasuk');

                $data[COL_KDSTATUS] = $this->input->post(COL_KDSTATUS);
                $data[COL_FORWARDEDAT] = date("Y-m-d H:i:s");
                $opd = $this->input->post(COL_KDOPD);

                $rstatus = $this->db->where(COL_KDSTATUS, $data[COL_KDSTATUS])->get(TBL_MSTATUS)->row_array();
                if($rstatus && $rstatus[COL_REQUIREOPD] == 1) {
                    if(count($opd) == 0) {
                        $resp['error'] = "Wajib pilih OPD / Instansi terkait!";
                        $resp['success'] = 0;
                        $this->db->trans_rollback();
                        echo json_encode($resp);
                        return;
                    }

                    $dataopd = array();
                    foreach($opd as $o) {
                        $data_clone = $rdata;
                        unset($data_clone[COL_NAME]);
                        unset($data_clone["MName"]);
                        $data_clone[COL_KDSURAT] = null;
                        $data_clone[COL_KDSUMBER] = $rdata[COL_KDSURAT];
                        $data_clone[COL_KDOPD] = $o;
                        $data_clone[COL_KDSTATUS] = $data[COL_KDSTATUS];
                        $dataopd[] = $data_clone;
                    }

                    if(!$this->db->insert_batch(TBL_TSURAT, $dataopd)){
                        $resp['error'] = 1;
                        $resp['success'] = 0;
                        $this->db->trans_rollback();
                        echo json_encode($resp);
                        return;
                    }
                }

                if(!$this->db->where(COL_KDSURAT, $id)->update(TBL_TSURAT, $data)){
                    $resp['error'] = 1;
                    $resp['success'] = 0;
                    $this->db->trans_rollback();
                    echo json_encode($resp);
                    return;
                }

                $datalog = array(
                    COL_KDSURAT => $id,
                    COL_KDSTATUS => $data[COL_KDSTATUS],
                    COL_CREATEDAT => date("Y-m-d H:i:s"),
                    COL_CREATEDBY => $ruser[COL_USERNAME]
                );

                if (!$this->db->insert(TBL_TSURAT_LOGSTAT, $datalog)) {
                    $resp['error'] = 1;
                    $resp['success'] = 0;
                    $this->db->trans_rollback();
                    echo json_encode($resp);
                    return;
                }
            }
            $this->db->trans_commit();
            echo json_encode($resp);
        }else{
            $this->load->view('data/surat_form',$data);
        }
    }

    /*function suratmasuk_add() {
        $data['title'] = "Surat Masuk";
        $data['edit'] = FALSE;
        $data['tipe'] = SURAT_MASUK;

        if(!empty($_POST)){
            $config['upload_path'] = MY_UPLOADPATH;
            $config['allowed_types'] = UPLOAD_ALLOWEDTYPES;
            $config['max_size']	= 512000;
            $config['max_width']  = 1600;
            $config['max_height']  = 1600;
            $config['overwrite'] = FALSE;

            $this->load->library('upload',$config);
            if(!empty($_FILES["userfile"]["name"])) {
                if(!$this->upload->do_upload()){
                    $resp['error'] = 1;
                    $resp['success'] = 0;
                    echo json_encode($resp);
                    return;
                }
            }
            $dataupload = $this->upload->data();

            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = site_url('data/suratmasuk');
            $data = array(
                COL_KDOPD => $this->input->post(COL_KDOPD),
                COL_KDTIPE => SURAT_MASUK,
                COL_KDSTATUS => SURAT_STAT_BARU,
                COL_NMJUDUL => $this->input->post(COL_NMJUDUL),
                COL_NMPERIHAL => $this->input->post(COL_NMPERIHAL),
                COL_CREATEDAT => date("Y-m-d H:i:s"),
                COL_CREATEDBY => $ruser[COL_USERNAME]
            );
            if(!empty($dataupload) && $dataupload['file_name']) {
                $data[COL_NMFILE] = $dataupload['file_name'];
            }
            if(!$this->db->insert(TBL_TSURAT, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }else{
            $this->load->view('data/surat_form',$data);
        }
    }*/

    function komentar($id) {
        if(!empty($_POST)){
            $config['upload_path'] = MY_UPLOADPATH;
            $config['allowed_types'] = UPLOAD_ALLOWEDTYPES;
            $config['max_size']	= 512000;
            $config['max_width']  = 1600;
            $config['max_height']  = 1600;
            $config['overwrite'] = FALSE;

            $this->load->library('upload',$config);
            if(!empty($_FILES["userfile"]["name"])) {
                if(!$this->upload->do_upload()){
                    $resp['error'] = $this->upload->display_errors(); //"Gagal upload lampiran. Nama file : ".$_FILES["userfile"]["name"];
                    $resp['success'] = 0;
                    echo $this->upload->display_errors(); //json_encode($resp);
                    return;
                }
            }
            $dataupload = $this->upload->data();

            $ruser = GetLoggedUser();
            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $data = array(
                COL_KDSURAT => $id,
                COL_NMKOMENTAR => $this->input->post(COL_NMKOMENTAR),
                COL_CREATEDAT => date("Y-m-d H:i:s"),
                COL_CREATEDBY => $ruser[COL_USERNAME]
            );
            if(!empty($dataupload) && $dataupload['file_name']) {
                $data[COL_NMFILE] = $dataupload['file_name'];
            }
            if(!empty($ruser[COL_COMPANYID])) {
                $data[COL_KDOPD] = $ruser[COL_COMPANYID];
            }
            if(!$this->db->insert(TBL_TSURAT_KOMENTAR, $data)){
                $resp['error'] = 1;
                $resp['success'] = 0;
            }
            echo json_encode($resp);
        }
    }

    function komentar_load($id) {
        $ruser = GetLoggedUser();
        $rsurat = $this->db->where(COL_KDSURAT, $id)->get(TBL_TSURAT)->row_array();

        $this->db->select("'Comment' as Type, 0 as KdStatus, Name, NmKomentar, tsurat_komentar.NmFile, tsurat_komentar.CreatedAt, tsurat_komentar.CreatedBy");
        $this->db->join(TBL_USERINFORMATION,TBL_USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TSURAT_KOMENTAR.".".COL_CREATEDBY,"inner");
        $this->db->join(TBL_TSURAT,TBL_TSURAT.".".COL_KDSURAT." = ".TBL_TSURAT_KOMENTAR.".".COL_KDSURAT,"inner");
        /*if($rsurat) {
            $this->db->where("(KdSurat = ".$id." or KdSurat = ".($rsurat[COL_KDSUMBER]?$rsurat[COL_KDSUMBER]:-1).")");
        }
        else {
            $this->db->where(COL_KDSURAT, $id);
        }*/
        $this->db->where("((tsurat_komentar.KdSurat = ".($rsurat[COL_KDSUMBER]?$rsurat[COL_KDSUMBER]:-1)." or tsurat_komentar.KdSurat = ".$rsurat[COL_KDSURAT].") and (userinformation.CompanyID is null or tsurat.KdTipe = ".SURAT_KELUAR."))");
        if(!empty($ruser[COL_COMPANYID])) {
            $this->db->or_where("(tsurat_komentar.KdSurat = ".$rsurat[COL_KDSURAT]." and userinformation.CompanyID = ".$ruser[COL_COMPANYID].")");
        }

        /*if(!empty($ruser[COL_COMPANYID])) {
            $this->db->where("(userinformation.CompanyID is null or userinformation.CompanyID = ".$ruser[COL_COMPANYID].")");
        }*/
        $this->db->order_by(COL_CREATEDAT, 'desc');
        $query_comment = $this->db->get(TBL_TSURAT_KOMENTAR);
        $comments = $query_comment->result_array();

        $this->db->select("'Log' as Type, tsurat_logstat.KdStatus, Name, NmStatus as NmKomentar, '' as NmFile, tsurat_logstat.CreatedAt, tsurat_logstat.CreatedBy");
        $this->db->join(TBL_USERINFORMATION,TBL_USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TSURAT_LOGSTAT.".".COL_CREATEDBY,"inner");
        $this->db->join(TBL_MSTATUS,TBL_MSTATUS.'.'.COL_KDSTATUS." = ".TBL_TSURAT_LOGSTAT.".".COL_KDSTATUS,"inner");
        $this->db->join(TBL_TSURAT,TBL_TSURAT.".".COL_KDSURAT." = ".TBL_TSURAT_LOGSTAT.".".COL_KDSURAT,"inner");
        /*if($rsurat) {
            $this->db->where("(KdSurat = ".$id." or KdSurat = ".($rsurat[COL_KDSUMBER]?$rsurat  [COL_KDSUMBER]:-1).")");
        }
        else {
            $this->db->where(COL_KDSURAT, $id);
        }
        if(!empty($ruser[COL_COMPANYID])) {
            $this->db->where("(userinformation.CompanyID is null or userinformation.CompanyID = ".$ruser[COL_COMPANYID].")");
        }*/
        $this->db->where("((tsurat_logstat.KdSurat = ".($rsurat[COL_KDSUMBER]?$rsurat[COL_KDSUMBER]:-1)." or tsurat_logstat.KdSurat = ".$rsurat[COL_KDSURAT].") and (userinformation.CompanyID is null or tsurat.KdTipe = ".SURAT_KELUAR."))");
        if(!empty($ruser[COL_COMPANYID])) {
            $this->db->or_where("(tsurat_logstat.KdSurat = ".$rsurat[COL_KDSURAT]." and userinformation.CompanyID = ".$ruser[COL_COMPANYID].")");
        }
        $this->db->order_by(COL_CREATEDAT, 'desc');
        $query_log = $this->db->get(TBL_TSURAT_LOGSTAT);
        $logs = $query_log->result_array();

        $comments = array_merge($comments, $logs);
        array_multisort(array_column($comments, COL_CREATEDAT), SORT_DESC, $comments);

        $data['comments'] = $comments;
        $this->load->view('data/_comments',$data);
    }

    function suratkeluar() {
        $ruser = GetLoggedUser();
        $data['title'] = 'Surat Keluar';
        $data['index_url'] = 'data/suratkeluar';
        $data['form_url'] = 'suratkeluar-form';
        $data['allowcreate'] = in_array($ruser[COL_ROLEID], allowCreateSuratKeluar());

        $isload = $this->input->get("load");
        $isAjaxCount = $this->input->get("ajaxcount");
        $page = $this->input->get("page") ? $this->input->get("page") : 1;
        $offset = ($page-1)*25;

        $data['page'] = $page;
        $data['offset'] = $offset;

        $query = $this->db;
        if($ruser[COL_ROLEID] != ROLEADMIN) {
            $allowed_roles =  $this->db->select(COL_KDSTATUS)->where(COL_KDROLE, $ruser[COL_ROLEID])->from(TBL_MSTATUS_ROLE)->get_compiled_select();
            $query->where(COL_KDSTATUS." IN ($allowed_roles)", NULL, FALSE);
        }
        $query->select("*, (select CreatedAt from tsurat_logstat inner join userinformation i on i.UserName = tsurat_logstat.CreatedBy where (tsurat_logstat.KdSurat = tsurat.KdSurat or tsurat_logstat.KdSurat = tsurat.KDSumber) and (i.CompanyID is null or ".(!empty($ruser[COL_COMPANYID])?"i.CompanyID = '".$ruser[COL_COMPANYID]."'":"1=1").") order by tsurat_logstat.CreatedAt desc limit 1) as Timestamp");
        $query->join(TBL_USERINFORMATION,TBL_USERINFORMATION.'.'.COL_USERNAME." = (select tsurat_logstat.CreatedBy from tsurat_logstat inner join userinformation i on i.UserName = tsurat_logstat.CreatedBy where (tsurat_logstat.KdSurat = tsurat.KdSurat or tsurat_logstat.KdSurat = tsurat.KDSumber) and (i.CompanyID is null or ".(!empty($ruser[COL_COMPANYID])?"i.CompanyID = '".$ruser[COL_COMPANYID]."'":"1=1").") order by tsurat_logstat.CreatedAt desc limit 1)","inner");
        if(!empty($ruser[COL_COMPANYID])) {
            $query->where(TBL_TSURAT.".".COL_KDOPD, $ruser[COL_COMPANYID]);
        }
        else {
            //$this->db->where(TBL_TSURAT.".".COL_KDOPD, null);
        }
        $query->where(COL_KDTIPE, SURAT_KELUAR);
        $query->where(COL_DELETED, null);
        $query->order_by(COL_FORWARDEDAT, 'desc');
        $query->order_by(COL_CREATEDAT, 'desc');
        if($isload) {

            $data['res'] = $query->get(TBL_TSURAT, $offset, 25)->result_array();
            $this->load->view('data/surat_partial', $data);
        }
        else if($isAjaxCount) {
            $count = $query->get(TBL_TSURAT)->num_rows();
            echo number_format($count, 0);
            return;
        }
        else {
            $data['countall'] = $query->count_all_results(TBL_TSURAT);
            $this->load->view('data/surat', $data);
        }
    }

    function suratkeluar_form($mode, $id=0) {
        $ruser = GetLoggedUser();

        if($mode == MODE_CREATE/* || $mode == MODE_EDIT*/ || $mode == MODE_DELETE) {
            if(!in_array($ruser[COL_ROLEID], allowCreateSuratKeluar())) {
                show_404();
                return;
            }
        }

        $data['title'] = "Surat Keluar";
        $data['edit'] = $mode != MODE_CREATE;
        $data['mode'] = $mode;
        $data['tipe'] = SURAT_KELUAR;
        $data['redirect'] = site_url('data/suratkeluar');
        $data['allowcomment'] = true;
        $data['ruser'] = $ruser;

        $rdata = $data['data'] = null;
        if($mode != MODE_CREATE) {
            $rdata = $data['data'] = $this->db
                ->select("tsurat.*, userinformation.Name, u2.Name as MName")
                ->join(TBL_USERINFORMATION,TBL_USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TSURAT.".".COL_CREATEDBY,"inner")
                ->join(TBL_USERINFORMATION." u2",'u2.'.COL_USERNAME." = ".TBL_TSURAT.".".COL_CREATEDBY,"inner")
                ->where(COL_KDSURAT, $id)->get(TBL_TSURAT)->row_array();
            if(empty($rdata)){
                show_404();
                return;
            }

            /* Jika sudah di disposisi tidak bisa delete */
            if($mode == MODE_DELETE && $rdata[COL_KDSTATUS] != 1) {
                show_404();
                return;
            }

            /* Jika status sudah tidak sesuai role tidak bisa comment */
            $role =  $this->db
                ->select(COL_KDSTATUS)
                ->where(array(COL_KDROLE=>$ruser[COL_ROLEID], COL_KDSTATUS=>$rdata[COL_KDSTATUS]))
                ->get(TBL_MSTATUS_ROLE)
                ->row_array();
            if(empty($role) && $ruser[COL_ROLEID] != ROLEADMIN) {
                $data['allowcomment'] = false;
            }
        }


        if(!empty($_POST)){
            $config['upload_path'] = MY_UPLOADPATH;
            $config['allowed_types'] = UPLOAD_ALLOWEDTYPES;
            $config['max_size']	= 512000;
            $config['max_width']  = 1600;
            $config['max_height']  = 1600;
            $config['overwrite'] = FALSE;

            $resp = array();
            $resp['error'] = 0;
            $resp['success'] = 1;
            $resp['redirect'] = $data['redirect'];
            $data = array(
                COL_KDTIPE => SURAT_KELUAR,
                COL_KDSTATUS => SURAT_KELUAR_STAT_BARU
            );

            $this->load->library('upload', $config);
            if (!empty($_FILES["userfile"]["name"])) {
                if (!$this->upload->do_upload()) {
                    $resp['error'] = 1;
                    $resp['success'] = 0;
                    echo json_encode($resp);
                    return;
                }

                $dataupload = $this->upload->data();
                if (!empty($dataupload) && $dataupload['file_name']) {
                    $data[COL_NMFILE] = $dataupload['file_name'];
                }
            }

            $this->db->trans_begin();
            if($mode == MODE_CREATE) {
                if(!empty($ruser[COL_COMPANYID])) $data[COL_KDOPD] = $ruser[COL_COMPANYID];
                $data[COL_NMJUDUL] = $this->input->post(COL_NMJUDUL);
                $data[COL_NMPERIHAL] = $this->input->post(COL_NMPERIHAL);
                $data[COL_CREATEDAT] = date("Y-m-d H:i:s");
                $data[COL_CREATEDBY] = $ruser[COL_USERNAME];

                if (!$this->db->insert(TBL_TSURAT, $data)) {
                    $resp['error'] = 1;
                    $resp['success'] = 0;
                    $this->db->trans_rollback();
                    echo json_encode($resp);
                    return;
                }

                $datalog = array(
                    COL_KDSURAT => $this->db->insert_id(),
                    COL_KDSTATUS => $data[COL_KDSTATUS],
                    COL_CREATEDAT => date("Y-m-d H:i:s"),
                    COL_CREATEDBY => $ruser[COL_USERNAME]
                );

                if (!$this->db->insert(TBL_TSURAT_LOGSTAT, $datalog)) {
                    $resp['error'] = 1;
                    $resp['success'] = 0;
                    $this->db->trans_rollback();
                    echo json_encode($resp);
                    return;
                }
            }
            else if($mode == MODE_EDIT) {
                $data[COL_NMJUDUL] = $this->input->post(COL_NMJUDUL);
                $data[COL_NMPERIHAL] = $this->input->post(COL_NMPERIHAL);
                $data[COL_MODIFIEDAT] = date("Y-m-d H:i:s");
                $data[COL_MODIFIEDBY] = $ruser[COL_USERNAME];

                if(!$this->db->where(COL_KDSURAT, $id)->update(TBL_TSURAT, $data)){
                    $resp['error'] = 1;
                    $resp['success'] = 0;
                    $this->db->trans_rollback();
                    echo json_encode($resp);
                    return;
                }
            }
            else if($mode == MODE_DELETE) {
                $data[COL_DELETED] = true;
                $data[COL_DELETEDAT] = date("Y-m-d H:i:s");
                $data[COL_DELETEDBY] = $ruser[COL_USERNAME];

                if(!$this->db->where(COL_KDSURAT, $id)->update(TBL_TSURAT, $data)){
                    $resp['error'] = 1;
                    $resp['success'] = 0;
                    $this->db->trans_rollback();
                    echo json_encode($resp);
                    return;
                }
            }
            else if($mode == MODE_FORWARD) {
                $resp['redirect'] = site_url('data/suratkeluar');

                $opd = $this->input->post(COL_KDOPD);
                if(count($opd) <= 0) $data[COL_KDSTATUS] = $this->input->post(COL_KDSTATUS);
                else $data[COL_KDSTATUS] = $rdata[COL_KDSTATUS];
                $data[COL_FORWARDEDAT] = date("Y-m-d H:i:s");


                $rstatus = $this->db->where(COL_KDSTATUS, $this->input->post(COL_KDSTATUS))->get(TBL_MSTATUS)->row_array();
                if($rstatus && $rstatus[COL_REQUIREOPD] == 1) {
                    if(count($opd) == 0) {
                        $resp['error'] = "Wajib pilih OPD / Instansi terkait!";
                        $resp['success'] = 0;
                        $this->db->trans_rollback();
                        echo json_encode($resp);
                        return;
                    }

                    $dataopd = array();
                    foreach($opd as $o) {
                        $data_clone = $rdata;
                        unset($data_clone[COL_NAME]);
                        unset($data_clone["MName"]);
                        $data_clone[COL_KDSURAT] = null;
                        $data_clone[COL_KDSUMBER] = $rdata[COL_KDSURAT];
                        $data_clone[COL_KDOPD] = $o;
                        $data_clone[COL_KDTIPE] = SURAT_MASUK;
                        $data_clone[COL_KDSTATUS] = $this->input->post(COL_KDSTATUS);
                        $dataopd[] = $data_clone;
                    }

                    if(!$this->db->insert_batch(TBL_TSURAT, $dataopd)){
                        $resp['error'] = 1;
                        $resp['success'] = 0;
                        $this->db->trans_rollback();
                        echo json_encode($resp);
                        return;
                    }
                }

                if(!$this->db->where(COL_KDSURAT, $id)->update(TBL_TSURAT, $data)){
                    $resp['error'] = 1;
                    $resp['success'] = 0;
                    $this->db->trans_rollback();
                    echo json_encode($resp);
                    return;
                }

                $datalog = array(
                    COL_KDSURAT => $id,
                    COL_KDSTATUS => $this->input->post(COL_KDSTATUS),
                    COL_CREATEDAT => date("Y-m-d H:i:s"),
                    COL_CREATEDBY => $ruser[COL_USERNAME]
                );

                if (!$this->db->insert(TBL_TSURAT_LOGSTAT, $datalog)) {
                    $resp['error'] = 1;
                    $resp['success'] = 0;
                    $this->db->trans_rollback();
                    echo json_encode($resp);
                    return;
                }
            }
            $this->db->trans_commit();
            echo json_encode($resp);
        }else{
            $this->load->view('data/surat_form',$data);
        }
    }

    function suratfwd() {
        $ruser = GetLoggedUser();
        $data['title'] = 'Surat Diteruskan';
        $data['index_url'] = 'data/suratfwd';
        $data['form_url'] = 'suratfwd-form';
        $data['allowcreate'] = false;

        $isload = $this->input->get("load");
        $page = $this->input->get("page") ? $this->input->get("page") : 1;
        $offset = ($page-1)*25;

        $data['page'] = $page;
        $data['offset'] = $offset;

        $query = $this->db;
        $query->select("*, (select CreatedAt from tsurat_logstat inner join userinformation i on i.UserName = tsurat_logstat.CreatedBy where (tsurat_logstat.KdSurat = tsurat.KdSurat or tsurat_logstat.KdSurat = tsurat.KDSumber) and (i.CompanyID is null or ".(!empty($ruser[COL_COMPANYID])?"i.CompanyID = '".$ruser[COL_COMPANYID]."'":"1=1").") order by tsurat_logstat.CreatedAt desc limit 1) as Timestamp");
        $query->join(TBL_USERINFORMATION,TBL_USERINFORMATION.'.'.COL_USERNAME." = (select tsurat_logstat.CreatedBy from tsurat_logstat inner join userinformation i on i.UserName = tsurat_logstat.CreatedBy where (tsurat_logstat.KdSurat = tsurat.KdSurat or tsurat_logstat.KdSurat = tsurat.KDSumber) and (i.CompanyID is null or ".(!empty($ruser[COL_COMPANYID])?"i.CompanyID = '".$ruser[COL_COMPANYID]."'":"1=1").") order by tsurat_logstat.CreatedAt desc limit 1)","inner");
        $query->join(TBL_MSTATUS,TBL_MSTATUS.".".COL_KDSTATUS." = ".TBL_TSURAT.".".COL_KDSTATUS,"inner");
        $query->join(TBL_MTIPE,TBL_MTIPE.".".COL_KDTIPE." = ".TBL_TSURAT.".".COL_KDTIPE,"inner");
        $query->where(COL_DELETED, null);
        $query->where("tsurat.KdSurat in (select KdSurat from tsurat_logstat where tsurat_logstat.CreatedBy = '".$ruser[COL_USERNAME]."')");
        $query->order_by("(select CreatedAt from tsurat_logstat where tsurat_logstat.CreatedBy = '".$ruser[COL_USERNAME]."' order by tsurat_logstat.CreatedAt desc limit 1)", "desc");
        //$data['res'] = $this->db->get(TBL_TSURAT)->result_array();
        //echo $this->db->last_query();
        //return;
        if($isload) {

            $data['res'] = $query->get(TBL_TSURAT, $offset, 25)->result_array();
            $this->load->view('data/suratfwd_partial', $data);
        }
        else {
            $data['countall'] = $query->count_all_results(TBL_TSURAT);
            $this->load->view('data/surat', $data);
        }

    }
}