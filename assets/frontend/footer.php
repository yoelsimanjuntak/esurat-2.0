</div>
</section>
</div>
</div>
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; <?=date("Y")?> MARTOBA</strong>. Strongly developed by <b>Partopi Tao</b>.
</footer>
</div>

<!-- Bootstrap 3.3.6 -->
<script src="<?=base_url()?>assets/adminlte/bootstrap/js/bootstrap.js"></script>
<!-- FastClick -->
<script src="<?=base_url()?>assets/adminlte/plugins/fastclick/fastclick.js"></script>

<script src="<?=base_url()?>assets/adminlte/plugins/morris/raphael-min.js"></script>
<script src="<?=base_url()?>assets/adminlte/plugins/morris/morris.min.js"></script>
<script src="<?=base_url()?>assets/adminlte/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url()?>assets/adminlte/dist/js/app.min.js"></script>

<script>
    var area = new Morris.Area({
        element: 'visitor-chart',
        resize: true,
        data: [
            {y: '2018 Q1', item1: 266, item2: 266},
            {y: '2018 Q2', item1: 277, item2: 229},
            {y: '2018 Q3', item1: 491, item2: 196},
            {y: '2018 Q4', item1: 376, item2: 359}
        ],
        xkey: 'y',
        ykeys: ['item1', 'item2'],
        labels: ['Chrome', 'Firefox'],
        lineColors: ['#a0d0e0', '#3c8dbc'],
        hideHover: 'auto'
    });

    $("#calendar").datepicker().datepicker('update', new Date(<?=date('Y')?>,<?=date('m')?>-1,<?=date('d')?>));
    $('a[href="<?=current_url()?>"]').addClass('active').parents('li').addClass('active');
</script>
</body>