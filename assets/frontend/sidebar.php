<div class="col-md-4">
    <div class="box box-solid bg-yellow-gradient">
        <div class="box-header ui-sortable-handle" style="cursor: move;">
            <i class="fa fa-calendar"></i>
            <h3 class="box-title">Kalender</h3>

            <div class="pull-right box-tools">
                <button type="button" class="btn btn-warning btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>

        <div class="box-body no-padding" style="display: block;">
            <!--The calendar -->
            <div id="calendar" style="width: 100%">
            </div>
        </div>
    </div>

    <div class="box box-solid">
        <div class="box-header ui-sortable-handle" style="cursor: move;">
            <i class="fa fa-th"></i>
            <h3 class="box-title">Statistik Pengunjung</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body border-radius-none">
            <div class="chart" id="visitor-chart" style="height: 250px; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">

            </div>
        </div>
    </div>
</div>