<?php $this->load->view('frontend/header') ?>
<div class="col-md-8">
    <div class="box box-solid">
        <div class="box-header with-border">
            <i class="fa fa-image"></i>
            <h3 class="box-title">Galeri</h3>
        </div>
        <div class="box-body">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                    <!--<li data-target="#carousel-example-generic" data-slide-to="3" class=""></li>-->
                </ol>
                <div class="carousel-inner">
                    <!--<div class="item active">
                                        <img src="<?=MY_IMAGEURL?>/slide/1.jpg" alt="SIDAMA">

                                        <div class="carousel-caption">
                                            First Slide
                                        </div>
                                    </div>-->
                    <div class="item active">
                        <img src="<?=MY_IMAGEURL?>/slide/1.jpeg" alt="MARTOBA">
                    </div>
                    <div class="item">
                        <img src="<?=MY_IMAGEURL?>/slide/2.jpeg" alt="MARTOBA">
                    </div>
                    <div class="item">
                        <img src="<?=MY_IMAGEURL?>/slide/3.jpeg" alt="MARTOBA">
                    </div>
                </div>
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                    <span class="fa fa-angle-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                    <span class="fa fa-angle-right"></span>
                </a>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('frontend/sidebar') ?>
<?php $this->load->view('frontend/footer') ?>