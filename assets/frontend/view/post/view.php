<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 15/10/2018
 * Time: 20:20
 */
?>
<?php $this->load->view('frontend/header') ?>
    <div class="col-sm-8">
        <div class="box box-widget">
            <div class="box-header with-border">
                <div class="user-block">
                    <span class="username" style="margin-left: 0px !important;"><a href="javascript:void(0)" style="text-transform: uppercase"><?=$data[COL_POSTTITLE]?></a></span>
                    <span class="description" style="margin-left: 0px !important; font-style: italic">Dibuat oleh <?=$data[COL_NAME]?> pada <?=date('d M Y', strtotime($data[COL_POSTDATE]))?></span>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="text-align: center">
                <img class="img-responsive pad" style="margin: auto" src="<?=!empty($data[COL_FILENAME])?MY_UPLOADURL.$data[COL_FILENAME]:MY_NOIMAGEURL?>" alt="<?=$data[COL_POSTTITLE]?>">
            </div>
            <!-- /.box-body -->
            <div class="box-footer box-comments">
                <div class="box-comment">
                    <div class="comment-text" style="text-align: justify; margin-left: 0px !important;">
                        <?=$data[COL_POSTCONTENT]?>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <a href="<?=site_url('post/archive')?>" class="btn btn-default btn-flat pull-right"><i class="fa fa-arrow-left"></i>&nbsp;Kembali</a>
            </div>
        </div>
    </div>
<?php $this->load->view('frontend/sidebar') ?>
<?php $this->load->view('frontend/footer') ?>