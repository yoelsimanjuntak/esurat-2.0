<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 04/10/2018
 * Time: 11:48
 */
?>
<?php $this->load->view('frontend/header') ?>
    <div class="col-sm-8">

        <div class="box box-solid">
            <div class="box-header with-border">
                <h4>Pelaksanaan Kegiatan 10 Program Pokok PKK</h4>
            </div>
            <div class="box-body">
                <div class="col-sm-12">
                    <p style="text-align: justify">
                        Pelaksanaan Kegiatan 10 Program Pokok PKK melalui Kegiatan Pembinaan pada saat
                        supervisi yang telah dilakukan pada bulan April 2018 yang dilakukan oleh POKJAI s/d IV
                        yang bekerjasama dengan OPD terkait. Seluruh kegiatan yang telah dilaksanakan pada saat
                        Supervisi selanjutnya dimonitoring pada bulan Agustus 2018 sebagai berikut :
                    </p>
                    <ol style="text-align: justify">
                        <li>
                            <b>Kelompok Kerja (POKJA) I :</b>
                            <ul>
                                <li>Mengadakan Penyuluhan tentang Kadarkum sekaligus mengarahkan Pembentukan Kelompok kadarkum.</li>
                                <li>Mengadakan penyuluhan tentang trafficking.</li>
                                <li>Mengarahkan TP. PKK desa agar membina dan membentuk kelompok lansia dan
                                    mengadakan kegiatan lansia dan Posyandu lansia.</li>
                                <li>Mengadakan pembinaan kepada warga masyarakat yang mempunyai anak balita
                                    agar aktif mengikuti kegiatan di kelompok BKB, serta meningkatakan Kualitas
                                    kader BKB dalam melaksanakan tugasnya masing-masing.</li>
                                <li>Mengadakan pembinaan kepada Kelompok Bina Keluarga Balita (BKB) dan Bina
                                    Keluarga Remaja (BKR) untuk menjadi “Orang Hebat” demi generasi yang lebih
                                    berkualitas.</li>
                                <li>Bersama dengan Dinas Sosial dan Dinas Catatan Sipil turun melakukan pembinaan
                                    ke desa – desa percontohan.</li>
                                <li>Pemberian bingkisan kepada peserta Koor Lansia.</li>
                            </ul>
                        </li>
                        <br />
                        <li>
                            <b>Kelompok Kerja (POKJA) II :</b>
                            <ul>
                                <li>Mengadakan penyuluhan tentang pentingnya Pendidikan Anak Usia Dini (PAUD).</li>
                                <li>Mengarahkan Tim Penggerak PKK Desa agar menyediakan taman bacaan dalam
                                    upaya meningkatkan kecerdasan warga desa binaan.</li>
                                <li>Mengarahkan warga yang telah dilatih agar memanfaatkan keterampilannya untuk
                                    Peningkatan Pendapatan Keluarga.</li>
                                <li>Memberikan bantuan Lunch Box kepada kelompok PAUD.</li>
                                <li>Memberikan bantuan Buku Cerita kepada anak SD.</li>
                                <li>Bersama dengan Dinas Pendidikan, Dinas Koperasi, Perdagangan dan Perindustrian
                                    turun melakukan pembinaan ke desa-desa percontohan.</li>
                            </ul>
                        </li>
                        <br />
                        <li>
                            <b>Kelompok Kerja (POKJA) III :</b>
                            <ul>
                                <li>Mensosialisasikan penganeka ragaman makanan pokok agar tidak tergantung
                                    kepada beras.</li>
                                <li>Mengadakan penyuluhan tentang Pedoman Umum Gizi Seimbang (PUGS) dalam
                                    rangka mewujudkan keluarga sadar Gizi (Kadarzi) dengan mengkonsumsi makanan
                                    beragam, bergizi, seimbang dan aman (B2SA).</li>
                                <li>Mengadakan penyuluhan tentang pemanfaatan lahan pekarangan dengan menanami
                                    Warung Hidup, Toga, Sayur Mayur untuk menghemat biaya dan menambah
                                    penghasilan keluarga.</li>
                                <li>Mengadakan penyuluhan tentang rumah sehat dan layak huni.</li>
                                <li>Mengadakan penyuluhan bercocok tanam dengan system hidroponik.</li>
                                <li>Melakukan penyuluhan gerakan makan Makanan Sehat dan Bergizi.</li>
                                <li>Melakukan pembinaan/demo masak masakan sehat dari bahan serba ikan (Pesmol
                                    dan acar kuning).</li>
                                <li>Pemberian bantuan makanan tambahan kepada anak PAUD dan SD berupa roti dan
                                    susu.</li>
                            </ul>
                        </li>
                        <br />
                        <li>
                            <b>Kelompok Kerja (POKJA) IV :</b>
                            <ul>
                                <li>Membina dan mengarahkan para kader Posyandu agar lebih meningkatkan kualitas
                                    pelayanan di Posyandu.</li>
                                <li>Mengarahkan Kader Posyanduagar dapat melaksanakan Posyandu Lansia.</li>
                                <li>Mengadakan penyuluhan tentang pentingnya pembuatan saluran air limbah jamban
                                    keluarga dan tempat pembuangan sampah dalam upaya melestarikanlingkungan
                                    hidup.</li>
                                <li>Memberikan penyuluhan tentang pentingnya mengikuti program keluarga
                                    berencana, serta mengajak masyarakat agar gemar menabung demi masa depan
                                    yang lebih baik.</li>
                                <li>Membina Ketua - Ketua kelompok Dasa Wisma agar mampu melaksanakan tugas
                                    dan fungsinya serta mengisi buku catatan Kelompok Dasa Wisma.</li>
                                <li>Membina dan mengadakan penyuluhan pentingnya kebiasaan mencuci tangan
                                    kepada Anak PAUD dan SD.</li>
                                <li>Memberikan bantuan alat PHBS kepada kelompok PAUD dan anak SD.</li>
                                <li>Membina dan mengadakan penyuluhan tentang bahaya penyalahgunaan
                                    NARKOBA kepada warga desa.</li>
                                <li>Membina dan mengadakan penyuluhan tentang pentingnya pemeriksaan organ
                                    reproduksi melalui IVA Test kepada wanita usia subur (WUS).</li>
                                <li>Bersama dengan Dinas Kesehatan, dan Kantor KB turun melakukan pembinaan ke
                                    desa-desa percontohan.</li>
                            </ul>
                        </li>
                        <br />
                        <li>
                            <b>Bidang Sekretariat TP PKK Kabupaten :</b>
                            <ul>
                                <li>Membina dan mengarahkan pengisian Administrasi Buku Wajib PKK (6 buku) dan
                                    pengisisan data umum, data pokja I s/d IV.</li>
                                <li>Pengarsipan surat masuk dan surat keluar secara tertib dan teratur.</li>
                                <li>Pengisian buku catatan Ketua PKK Dusun ketua Kelompok Dasa Wisma.</li>
                                <li>Bekerjasama dengan Dinas Pemberdayaan Masyarakat, Desa, Perempuan dan
                                    Perlindungan Anak (DPMDP2A) Kabupaten Humbang Hasundutan melaksanakan
                                    pembinaan dan Pelatihan Kesekretariatan tentang tugas pokok dan fungsi dari
                                    masing masing pengurus, yaitu:
                                    <ol>
                                        <li>Ketua, Sekretaris, Bendahara dan Tim Penggerak PKK Desa Binaan.
                                            Membina Ketua dan Anggota Pokja I s/d IV agar mengetahui tentang 10
                                            (sepuluh) Program Pokok PKK dan program pengembangannya.</li>
                                        <li>Melaksanakan pelatihan kepada setiap Pokja tata cara menyusun administrasi
                                            masing-masing Pokja dalam buku administrasi.</li>
                                        <li>Melaksanakan pembinaan kepada kelompok PKK Dusun dan Dasa Wisma
                                            tentang tugas pokok dan fungsinya serta tata cara penyusunan administrasi
                                            kelompok PKK Dusun dan Kelompok Dasa Wisma.</li>
                                    </ol>
                                </li>
                                <li>Sosialisasi tentang e-dasawisma kepada Pengurus TP. PKK Desa yang
                                    direncanakan akan diterapkan pada Tahun 2019 di Desa-desa Percontohan
                                    kabupaten Humbang Hasundutan.</li>
                            </ul>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('frontend/sidebar') ?>
<?php $this->load->view('frontend/footer') ?>