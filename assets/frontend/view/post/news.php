<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 04/10/2018
 * Time: 13:58
 */
?>
<?php $this->load->view('frontend/header') ?>
    <div class="col-sm-8">
        <div class="box box-widget">
            <div class="box-header with-border">
                <h4><?=$title?></h4>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <?php
                foreach($data as $n) {
                    ?>
                    <div class="attachment-block clearfix" style="position: relative">
                        <!--<img class="attachment-img" src="<?=!empty($n[COL_FILENAME])?MY_UPLOADURL.$n[COL_FILENAME]:MY_NOIMAGEURL?>" width="160px" style="box-shadow: 5px 5px 5px #dedede">-->
                        <span class="post-labels">
                            <a href="javascript:void(0)" rel="tag" ><?=date('d-m-Y', strtotime($n[COL_POSTDATE]))?></a>
                        </span>
                        <div class="block-image">
                            <div class="thumb">
                                <a href="<?=!empty($n[COL_FILENAME])?MY_UPLOADURL.$n[COL_FILENAME]:MY_NOIMAGEURL?>" style="background:url(<?=!empty($n[COL_FILENAME])?MY_UPLOADURL.$n[COL_FILENAME]:MY_NOIMAGEURL?>) no-repeat center center;background-size:cover" target="_blank"></a>
                            </div>
                        </div>

                        <div class="attachment-pushed" style="margin-left: 210px !important;">
                            <h4 class="attachment-heading" style="text-transform: uppercase; padding-bottom: 5px; border-bottom: 1px solid #dedede">
                                <a href="<?=site_url('post/view/'.$n[COL_POSTSLUG])?>">
                                    <?=$n[COL_POSTTITLE]?>
                                </a>
                            </h4>
                            <div class="attachment-text" style="margin-top: 5px">
                                <?php
                                $strippedcontent = strip_tags($n[COL_POSTCONTENT]);
                                ?>
                                <p style="text-align: justify">
                                    <?=strlen($strippedcontent) > 200 ? substr($strippedcontent, 0, 200) . "..." : $strippedcontent ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div class="box-footer">
            </div>
        </div>
    </div>
<?php $this->load->view('frontend/sidebar') ?>
<?php $this->load->view('frontend/footer') ?>