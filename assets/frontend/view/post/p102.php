<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 04/10/2018
 * Time: 10:08
 */
?>
<?php $this->load->view('frontend/header') ?>
    <div class="col-sm-8">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h4>Visi & Misi</h4>
            </div>
            <div class="box-body">
                <ul>
                    <li>
                        <p style="text-decoration: underline">VISI</p>
                        <blockquote>
                            <p>"Mewujudkan Humbang Hasundutan yang Hebat dan Bermentalitas Unggul"</p>

                            <!--<small>Someone famous in <cite title="Source Title">Source Title</cite></small>-->
                        </blockquote>
                        <blockquote>
                            <p>Humbang Hasundutan yang "HEBAT"</p>
                        </blockquote>
                        <table>
                            <tr>
                                <td valign="top"><b>H</b></td>
                                <td valign="top" style="width: 20px; text-align: center">=</td>
                                <td valign="top">Humbang Hasundutan Na Martuhan jala maduma (Peningkatan Keimanan Kesejahteraan dan Kualitas SDM dan Sumber Daya Alam) </td>
                            </tr>
                            <tr>
                                <td valign="top"><b>E</b></td>
                                <td valign="top" style="width: 20px; text-align: center">=</td>
                                <td valign="top">Eme Na Godang Tano Na Bidang (Mewujudkan Ketahanan Pangan)</td>
                            </tr>
                            <tr>
                                <td valign="top"><b>B</b></td>
                                <td valign="top" style="width: 20px; text-align: center">=</td>
                                <td valign="top">Bahen Murah Arga Ni Pupuk (Penyediaan Saprodi dan Alsintan)</td>
                            </tr>
                            <tr>
                                <td valign="top"><b>A</b></td>
                                <td valign="top" style="width: 20px; text-align: center">=</td>
                                <td valign="top">Asa Sinur Na Pinahan Gabe Na Niula (Peningkatan Ekonomi Kerakyatan)</td>
                            </tr>
                            <tr>
                                <td valign="top"><b>T</b></td>
                                <td valign="top" style="width: 20px; text-align: center">=</td>
                                <td valign="top">Ture Dalan Tu Huta Sahat Tu Balian Asa Langku Na Ni Ula Dohot Tiga-Tiga (Peningkatan Kualitas Infrastruktur)</td>
                            </tr>
                        </table>
                    </li>
                    <hr/>
                    <li>
                        <p style="text-decoration: underline">MISI</p>
                        <ul>
                            <li>Meningkatkan iman dan taqwa kepada Tuhan Yang Maha Esa.</li>
                            <li>Meningkatkan kualitas Sumber Daya Manusia dan Sumber Daya Alam</li>
                            <li>Meningkatkan tata kelola pemerintahan yang baik.</li>
                            <li>Meningkatkan Kedaulatan Pangan dan Ekonomi kerakyatan.</li>
                            <li>Meningkatkan ketersediaan infrastruktur dan pengembangan wilayah.</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
<?php $this->load->view('frontend/sidebar') ?>
<?php $this->load->view('frontend/footer') ?>