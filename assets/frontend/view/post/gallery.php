<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 04/10/2018
 * Time: 11:48
 */
?>
<?php $this->load->view('frontend/header') ?>
    <div class="col-sm-8">

        <div class="box box-solid">
            <div class="box-header with-border">
                <h4>Galeri</h4>
            </div>
            <div class="box-body">
                <?php
                foreach($data as $n) {
                    ?>
                    <div style="display: inline-block; position: relative; margin-bottom: 10px" class="col-sm-4">
                        <span class="post-labels" style="left: 5px !important;">
                            <a href="javascript:void(0)" rel="tag" ><?=date('d-m-Y', strtotime($n[COL_POSTDATE]))?></a>
                        </span>
                        <div class="block-image" style="width: 100% !important;">
                            <div class="thumb">
                                <a href="javascript: popUpImage('<?=$n[COL_POSTTITLE]?>', '<?=site_url('post/view-partial/'.$n[COL_POSTSLUG])?>')" title="<?=$n[COL_POSTTITLE]?>" style="background:url(<?=!empty($n[COL_FILENAME])?MY_UPLOADURL.$n[COL_FILENAME]:MY_NOIMAGEURL?>) no-repeat center center;background-size:cover" target="_blank"></a>
                            </div>
                        </div>
                    </div>
                <?php
                }
                ?>
            </div>
        </div>
    </div>
    <div class="modal fade modal-default" id="popUpDialog" role="dialog">
        <div class="modal-dialog" style="width: 1080px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-close"></i></span>
                    </button>
                    <h4 class="modal-title">Title</h4>
                </div>
                <div class="modal-body">
                    Content
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
<?php $this->load->view('frontend/sidebar') ?>
<?php $this->load->view('frontend/footer') ?>
<script>
    function popUpImage(title, url) {
        $('.modal-title', $("#popUpDialog")).html(title);
        $('#popUpDialog').modal('show');
        $("#popUpDialog").find(".modal-content").load(url, function() {

        });
    }
</script>